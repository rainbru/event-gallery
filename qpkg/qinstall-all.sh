#!/bin/sh

PUBLIC_SHARE=`/sbin/getcfg SHARE_DEF defPublic -d Public -f /etc/config/def_share.info`
WEB_SHARE=`/sbin/getcfg SHARE_DEF defWeb -d Qweb -f /etc/config/def_share.info`
LOG="/sbin/log_tool"

# from https://forum.qnap.com/viewtopic.php?t=110633
$LOG --append "Testing log" --type=1
#WEB="/var/www"      # Local test
WEB="/share/Web"    # QNAP install 
LOGFILE="$WEB/event-gallery-install-log/index.html"

start_log()
{
    echo "<pre>" > $LOGFILE
}

end_log()
{
    echo "</pre>" >> $LOGFILE
}

log()
{
    MSG=$1
    echo "$MSG" >> $LOGFILE
}

# Install
#rm -fr /web/Share/event-gallery
#ln -s /share/CACHEDEV1_DATA/.qpkg/event_gallery/ /web/Share/event-gallery

start_log
log "Testing log"
rm -fr /share/Web/event-gallery/
ln -s /share/CACHEDEV1_DATA/.qpkg/event_gallery/ /share/Web/event-gallery/
log "Another line"
end_log
