#!/bin/sh
CONF=/etc/config/qpkg.conf
QPKG_NAME="event-gallery"
QPKG_ROOT=`/sbin/getcfg $QPKG_NAME Install_Path -f ${CONF}`
APACHE_ROOT=`/sbin/getcfg SHARE_DEF defWeb -d Qweb -f /etc/config/def_share.info`

case "$1" in
  start)
      ENABLED=$(/sbin/getcfg $QPKG_NAME Enable -u -d FALSE -f $CONF)
      if [ "$ENABLED" != "TRUE" ]; then
          echo "$QPKG_NAME is disabled."
          exit 1
      fi
      rm -fr /share/Web/event-gallery/
      ln -s /share/CACHEDEV1_DATA/.qpkg/event_gallery/ /share/Web/event-gallery/
      ;;

  stop)
      rm -fr /share/Web/event-gallery/
      ;;

  restart)
    $0 stop
    $0 start
    ;;

  *)
    echo "Usage: $0 {start|stop|restart}"
    exit 1
esac

exit 0
