<?php


/** 
 * Read a list a comma-separated paths excluded from the indexing.
 *
 */
class ExcludedPath { 

    /** An array of excluded paths */
    private $paths;

    /** Defautl constructor
     *
     * \param $ini_file A possible alternative configuration file.
     *
     */
    function __construct($ini_file = 'eg.ini') {
        $exini = parse_ini_file ('eg.ini')['excluded_path'];
        $this->paths = explode(',', $exini);
    }

    /** Returns the number of excluded paths (the size of the internal array)
     *
     */
    function size() {
        return sizeof($this->paths);
    }

    /** Is the given path excluded
     *
     * \param $path the to-be-tested path.
     *
     */
    function isExcluded($path) {
        return in_array($path, $this->paths);
    }
}
