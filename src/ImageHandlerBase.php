<?php

/** 
 * The image handler base class. You must use a specialisation of this.
 * 
 * You should use ImageHandlerFactory to get the ImageHandler according to
 * the configuration file.
 * 
 * It is designed to be instanciated once for all 
 * before the image processing loop.
 * 
 */
abstract class ImageHandlerBase { 

    // All these members are accessible from specialized classes
    protected $gallerydir;    //!< The gallery directory
    protected $thumbsdir;     //!< The thumbs directory
    protected $thumbwidth;    //!< The thumbnail width
    protected $thumbheight;   //!< The thumbnail height
    protected $thumbquality;  //!< The thumbnail quality

    // All values associated with Resize+Layout utility
    protected $RZ_input;   //!< The RZ input directory
    protected $RZ_output;  //!< The RZ output directory
    protected $RZ_width;   //!< The output image width
    protected $RZ_height;  //!< The output image height
    protected $RZ_quality; //!< The thumbnail quality
    protected $RZ_layer;   //!< The layer filename
    
    /**
     * Constructor: it must keep all one-access vars to be able to
     * produce images fast.
     *
     * \param $ini_file is used to change config file location from unit tests.
     *
     */
    function __construct($ini_file = 'eg.ini') {
        $cfg = parse_ini_file ($ini_file);
        $this->gallerydir = $cfg['gallery_dir'];
        $this->thumbsdir = $cfg['thumbs_dir'];
        $this->thumbwidth = $cfg['thumbs_width'];
        $this->thumbheight = $cfg['thumbs_height'];
        $this->thumbquality = $cfg['thumbs_quality'];

        // RZ values
        $this->RZ_input   = $cfg['RZ_input'];
        $this->RZ_output  = $cfg['RZ_output'];
        $this->RZ_width   = $cfg['RZ_width'];
        $this->RZ_height  = $cfg['RZ_height'];
        $this->RZ_quality = $cfg['RZ_quality'];
        $this->RZ_layer   = $cfg['RZ_layer'];
    }

    /** 
     * Get the thumbnail path from the gallery one
     *
     * \param $img The original image path
     *
     */
    function getThumbPath($img) {
        return str_replace($this->gallerydir, $this->thumbsdir, $img);
    }

    /** Returns the Resize+Layout quality extracted from the configuration 
      * file in the class constructor
      *
      */
    function getRzQuality() {
        return $this->RZ_quality;
    }
    
    /**
     * Creates the given thumbnails directory and all its parents if
     * it doesn't exist.
     *
     * \param $dir The -to-be-created directory
     *
     */
    function createThumbDir($dir) {
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        if (!is_dir($dir)) {
            throw new Exception("'$dir' creation failed!");
        }
        
    }

    /** Returns an output path for Resize+Layout replacing input path
      *
      * Will replace RZ input path with output path.
      *
      */
    function getRzToOutputPath($inputPath) {
        //$outpath = str_replace($input, $output, $path);
        return str_replace($this->RZ_input, $this->RZ_output, $inputPath);
    }

    /** Change the Date/Time of a generated thumb
      *
      * The new date/time is taken from the EXIF data of the original photo.
      *
      */
    function changeThumbDate($photo, $thumb) {
        $exif = exif_read_data($photo, 0, true);
        if (isset($exif['EXIF'])) {
            $dto = $exif['EXIF']['DateTimeOriginal'];
            $idt = strtotime($dto);
            touch($thumb, $idt, $idt);
        }
        else {
            $msg = "Can't find EXIF data on source image '$photo'";
            throw new Exception($msg);
        }
    }

    /** Change the date/time of the output to be the same that intput
      *
      */
    function keepThumbDate($input, $output) {
        $mt = filemtime($input);
        touch($output, $mt, $mt);

    }

    /** Returns the 'DateTimeOriginal' EXIF value of the given photo */
    function getDto($img) {
        $exif = exif_read_data($img, 0, true);
        if (!isset($exif['EXIF'])) {
            echo "Can't get EXIF data for $img";
            return False;
        }
        return $exif['EXIF']['DateTimeOriginal'];
    }
    
    /** Must be reimplemented by subclasses
     *
     * \param $img the full path to to-be-resized image.
     *
     */
    abstract public function generateThumbnail($img);

    /** Must be reimplemented by subclasses
     *
     * \param $img the full path to to-be-resized image.
     * \param $img the $out filename
     *
     */
    abstract public function generateThumbnailWithLayer($img, $out);
}

?>
