<?php
require_once(dirname(__FILE__).'/DbFile.php');
require_once(dirname(__FILE__).'/LinkTo.php');
require_once(dirname(__FILE__).'/GetParam.php');
require_once(dirname(__FILE__).'/Utils.php');

include(__DIR__ . '/template.php');
setlocale(LC_ALL, "fr_FR", 'fr', 'fr_FR.UTF8', "French");

$crc = get_param("id");
$c = get_param("c");

if (isset($c)) {
    $loading = '<div id="loading"></div>';
} else {
    $loading = '';
}

$cfg = parse_ini_file ('eg.ini');
$brand = $cfg['brand'];

//link_to_debug();

$panier_lnk = "<a class='btn btn-primary' href='".
            link_to("/panier/?id=$crc&c=$c").
            "' role='button'>Panier</a>";

$df = new DbFile();
$gp = $df->getGalleryPath($crc);
$event = ucfirst(basename($gp));
$subevent = ucfirst(urldecode($c));

if (!$gp) {
    echo "Not found";
    exit();
}

set_from_cookie();

if (!$c) {
    print_subdirs();
    exit();
}

$hostname = $cfg['hostname'] . '?id=' . $crc;

$content = "";

$gdir = $cfg['gallery_dir'];
$gurl = $cfg['gallery_url'];
$tdir = $cfg['thumbs_dir'];
$turl = $cfg['thumbs_url'];


// Handle the show_hour flag from config file
$show_hour = $cfg['show_hour'];

// List subdirs
$title = $event . " - " . $brand;

// Enable/disable partial load
$pl_enable = $cfg['pl_enable']|false;
if (!$pl_enable) {
    $loading = fullLoad();
}

$tpl = new Template();
print $tpl->render('index', array(
    'TITLE'      => $title,
    'EVENT'      => event_breadcrumb(),
    'CONTENT'    => $content,
    'BRAND'      => $brand,
    'HOSTNAME'   => $hostname,
    'PANIER_LNK' => $panier_lnk,
    'PRICES'     => prices_inputs(),
    'LOADING'    => $loading
));

if ($pl_enable) {
    partialLoad();
}

function print_photo($photo) {
    global $turl, $gdir, $gurl, $tdir, $event, $subevent, $show_hour;
    
    $thumb = str_replace($gdir, $turl, $photo);
    $thumb_file = str_replace($gdir, $tdir, $photo);
    $photo = $gurl. str_replace($gdir, "", $photo);
    $bn = basename($photo);

    $datespan = "";
    if ($show_hour) {
        $date = date("H:i:s", filemtime($thumb_file));
        $datespan="&nbsp;<span class='img-date'>$date</span>";
    }

    $alt="Photo $bn dans la gallerie $event/$subevent";
    return "
        <div class='ih-item'><a onclick='javascript:showDetails(\"$photo\");'>
          <div class='img'><img src='$thumb' alt='$alt' class='zoomin img-thumbnail'></div>
            <div class='img-info'>
              <span class='img-name'>$bn</span>$datespan
        </div></a></div>";
}


function print_subdirs()
{
    global $crc, $gp, $brand, $c, $panier_lnk, $loading;

    $event = ucfirst(basename($gp));
    $subevent = ucfirst(urldecode($c));

    $title = $event." - " . $brand;
//    print"<h1>$event</h1>";
    
    $output="";
    $files = scandir($gp);
    foreach($files as $file)
    { 
        if ($file == '.' || $file == '..') { 
            continue; 
        }
        //        $link = "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        //        $link .= "&c=". urlencode($file);
        $link = link_to('?id=' . $crc."&c=". urlencode($file));
        $output = $output.("<p><a href='$link'>". ucfirst($file) . "</a>");
    }
    $tpl = new Template();
    print $tpl->render('index', array(
        'TITLE'      => $title,
        'BRAND'      => $brand,
        'EVENT'      => event_breadcrumb(),
        'CONTENT'    => $output,
        'PANIER_LNK' => $panier_lnk,
        'HOSTNAME'   => $hostname,
        'PRICES'     => prices_inputs(),
        'LOADING'    => $loading
));

}


// event_breadcrumb_element
// If link is not set (NULL), the element is active
function ebe($label, $link=null) {
    if ($link) {
        return "<li class='breadcrumb-item'><a href='$link'>$label</a></li>";
    } else {
        return "<li class='breadcrumb-item active' aria-current='page'>
$label</li>";
    }
}
    
function event_breadcrumb() {
    global $event, $subevent, $crc;
    if (!$subevent) {
        return ebe($event); // . ebe("aze");
    } else {
        return ebe($event, link_to('?id=' . $crc)) . ebe($subevent);
    }
}

/** Set the FROM cookie, used to print breadcrumb menu in panier page
  *
  */
function set_from_cookie()
{/*
    $ahref = "<a href='#'>aze</a>";
    setcookie("from", $ahref, time() + 3600, "/");
 */
}

function priceToData($price)
{
    return str_replace(',', '.', sprintf("%.2f €", $price));
}

/** Simply return prices for this gallery as form's input elements
 *
 */
function prices_inputs()
{
    global $crc, $cfg, $df, $gp;
    $prices = $df->getPrices($gp);
    $arr = json_decode($cfg['default_formats']);

    if ($arr == NULL) {
        throw new Exception("'default_formats' du fichier de configuration est NULL");
    }
    
    $pricesArray =  json_decode($prices);
    if ($pricesArray == NULL) {
        $pricesArray = $arr;
    }

    // Throws an exception if the config file default_format can't be read
    if (count($pricesArray) == 0) {
        throw new Exception("'default_formats' du fichier de configuration est NULL");
    }
    
    $ret='<table>';
    foreach ($pricesArray as $price) {
        $ret = $ret . '<tr>';
        $pstr = price_to_str($price[1]);
        // Handle empty comment (without parenthesis)
        if (strlen(trim($price[2])) > 0) {
            $ret = $ret . "<td><input type='radio' name='format' 
value='$price[0]' data='".priceToData($price[1])."'>&nbsp;$price[0]</input></td> <td align='right'> $pstr </td> <td>&nbsp;($price[2])</td>";
        } else {
            $ret = $ret . "<td><input type='radio' name='format' 
value='$price[0]' data='".priceToData($price[1])."'>&nbsp;$price[0]</input><td align='right'>$pstr</td>";

        }

        $ret = $ret . '</tr>';

    }
    return $ret."</table>";
}

/** Add the partial load script markup
  *
  */
function partialLoad(){
    global $cfg, $crc, $c;
    $pl_initial  =$cfg['pl_initial'];
    $pl_newload  =$cfg['pl_newload'];
    $pl_threshold=$cfg['pl_threshold'];

    if (!isset($pl_threshold)) {
        echo "Warning : pl_threshold is not set";
    }
    
    echo "<script>setThreshold($pl_threshold);</script>";
    echo "<script>partialLoad($pl_initial, $pl_newload,'$crc','$c');</script>";
    ob_flush(); 
    flush(); 
}

/** Add the partial load script markup
  *
  */
function fullLoad(){
    global $cfg, $c, $gp;

    $res = "";
    
    // $gp is the path to the gallery from local mountpoint (/home/...)
    // Change it to a public URL thing (https://)
    $gdir = $cfg['gallery_dir'];
    $gurl = $cfg['gallery_url'];
    $turl = $cfg['thumbs_url'];
    $tpath =  str_replace($gdir, $turl, $gp) . '/' . urldecode($c);

    $path = $gp  . '/' . urldecode($c);
    $file_ary = scandir($path);
    if (!$file_ary) {
        echo "Can't read '$path'. scandir() returned FALSE.";
    }

    //        echo("Adding $nb images from directory $path : \n");
    
    $gpath =  str_replace($gdir, $gurl, $gp) . '/' . urldecode($c);
    foreach ($file_ary as $file) {
        if ($file[0] == '.') { continue;  } 

        $fullpath = $gpath . '/' . $file;
        $thumbpath = $tpath . '/' . $file;
        
        $thumb="<div class='ih-item'><a onclick='javascript:showDetails(\"" .
              $fullpath . "\");'><div class='img'><img src='" .
              $thumbpath . "' id='" . $file . "'></img></a></div></div>";
        $res .=  $thumb;
    }
    return $res;
}

?>
