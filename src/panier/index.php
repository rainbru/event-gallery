<?php
include(__DIR__ . '/../template.php');
include(__DIR__ . '/../SendMail.php');
require_once(__DIR__.'/../GetParam.php');
require_once(__DIR__.'/../DbFile.php');
require_once(__DIR__.'/../LinkTo.php');


class FieldValidationState {
    public $error    = false; // Is this field in error state
    public $success  = false; // Is this field in success state
    public $previous = "";      // Previously entered content
    public $message  = "";      // The message to be printed to the end user

    function setInvalid($previous, $message) {
        $this->error    = true;
        $this->success  = false;
        $this->previous = $previous;
        $this->message  = $message;
    }

    function setValid($previous, $message) {
        $this->error    = false;
        $this->success  = true;
        $this->previous = $previous;
        $this->message  = $message;
    }

    function reset()  {
        $this->error    = false;
        $this->success  = false;
        $this->previous = "";
        $this->message  = "";
    }

    public function __construct() {
        $this->reset();
    }
}

/** A class to handle form validation result
  *
  * Must be passed to print_form.
  *
  * If you set an Error flag, you must set the Previous content 
  * for this field and the asociated error Message.
  *
  */
class SendFormValidation { 
    public $nameFVS;
    public $emailFVS;

    public function __construct() {
        $this->nameFVS  = new FieldValidationState();
        $this->emailFVS = new FieldValidationState();
        $this->mailFVS = new FieldValidationState();
        $this->telFVS = new FieldValidationState();
    }
}

/** The number of 13x18 photos found in the panier
  *
  * So, we can count the number of 13x18 photo we have and eventually know
  * if we have odd or even ones.
  *
  */
$nb13x18photos = 0;

$cfg = parse_ini_file ('../eg.ini');

$brand = $cfg['brand'];
$hostname = $cfg['hostname'];

$prix_total = 0;
$validator = new SendFormValidation;

// BreadCrumb variables
$crc = get_param("id");
$c = get_param("c");
$df = new DbFile();
$gp = $df->getGalleryPath($crc);
$event = ucfirst(basename($gp));
$subevent = ucfirst(urldecode($c));


$content = '<h2>Récapitulatif de votre commande</h2>';

// Handle mail test
if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['sendMail']))
    {
//        header("Location: #sendform");
        //        echo print_r($_POST, true);
        $content = '';
        $valid = true;
        
        if (! $_COOKIE["photos"]) {
            echo "Le panier est vide!";
            $content .= "<div class='alert alert-danger' role='alert'>
Votre panier est vide. </br>
Veuillez ajouter des photos et réessayer.
</div>";

        }

        // Name
        $name = $_POST['name'];
        if (empty($name)) {
            $validator->nameFVS->setInvalid($name, 'Le nom ne peut être vide.');
            $valid = false;
        } else if (strlen($name) < 4) {
            $validator->nameFVS->setInvalid($name, 'Le nom est trop court.');
            $valid = false;
        } else {
            $validator->nameFVS->setValid($name, 'Le nom est valide.');
        }

        // Email
        $email = $_POST['email'];
        if (empty($email)) {
            $validator->emailFVS->setInvalid($email, "L'email ne peut être vide.");
            $valid = false;
        } else  if (!strstr($email, '@')) {
            $validator->emailFVS->setInvalid($email, "L'email est invalide");
            $valid = false;
        } else {
            $validator->emailFVS->setValid($email, "L'email est valide");
        }

        // Mail
        $mail = $_POST['mail'];
        if (empty($mail)) {
            $validator->mailFVS->setInvalid($mail, "L'adresse ne peut être vide.");
        } else {
            $validator->mailFVS->setValid($mail, "L'adresse est valide.");
        }

        // Tel
        $tel = $_POST['tel'];
        if (empty($tel)) {
            $validator->telFVS->setInvalid($tel, "Le numéro de télephone ne peut être vide.");
            $valid = false;
        } else {
            $validator->telFVS->setValid($tel, "Le numéro de télephone est valide.");
        }
        

        
        $pc_content = photos_cookie_mail();

        $tpl = new Template();
        $render =  $tpl->render('mail/user', array(
            'NAME'         => $name,
            'PHOTO_TABLE'  => $pc_content,
            'TOTAL'        => $prix_total,
            'MAIL'         => $mail,
            'TEL'          => $tel
        ));
        
        
        
        if ($valid) {
            $replyto = "greg.woz@gmail.com";
            
            $sm_user = new SendMail;
            $sm_user->subject = "Votre commande greg-w.com";
            $sm_user->content = $render;
            $sent_user = $sm_user->send($replyto, false, $email);


            $tpl = new Template();
            $render =  $tpl->render('mail/admin', array(
                'NAME'         => $name,
                'PHOTO_TABLE'  => $pc_content,
                'TOTAL'        => $prix_total,
                'MAIL'         => $mail,
                'EMAIL'        => $email,
                'TEL'          => $tel
            ));

            
            $sm_admin = new SendMail;
            $sm_admin->subject = "Nouvelle commande event-gallery";
            $sm_admin->content = $render;
            $sent_admin = $sm_admin->send($replyto);

            if ($sent_user && $sent_admin) {
                setcookie("photos", "", time() + 3600, "/"); 
                print_success_template($email);
            } else {
                $content .= $pc_content;
                $content .= print_form($validator);
                print_template($content);
            }
        }
        
        exit;
    }


$content .= photos_cookie();
$content .= print_form($validator);

$title = 'Panier - '.$brand;

print_template($content);

class Image {
    public $id;       //!< The id in the array
    public $url;      //!< Full path (URL) to the image
    public $format;   //!< The format string
    public $options;  //!< All the options in one string
    public $price;    //!< The photo price
    public $qty;      //!< The quantity
}

function print_image(Image $img) {
    $fn = basename($img->url);
    $btnname="";
    $btnclass="";
    if ($img->qty > 1) {
        $btnname = "Reduire";
        $btnclass="btn-warning";
    } else {
        $btnname = "Supprimer";
        $btnclass="btn-danger";
    }
    $btn = "<button class='btn $btnclass' onclick='removeOne(\"$img->id\");location.reload();'>
       $btnname</button>";
    
    return "
<div class='row' id='photo-$img->id'>
  <div class='col-8'>
    <img class='img-fluid'src='$img->url'></img><br>
  </div>
  <div class='col-4'>
$fn<br>
              Format : $img->format<br>
              Quantité : $img->qty&nbsp;$btn<br>
              Prix : $img->price&nbsp;€<br>
</div>
            </div>";

}

function photos_cookie() {
    global $cfg, $prix_total, $nb13x18photos;
    
    $ret = "";
    $photos_cookie = $_COOKIE["photos"];
    $photo_arr = json_decode($photos_cookie, true);

    $gurl = $cfg['gallery_url'];
    $turl = $cfg['thumbs_url'];

    $current_id = 0;
    foreach ($photo_arr as $value) {
        //        $thumb_file = str_replace($gurl, $turl, $value['file']);
        $img = new Image();
        $img->id      = $current_id;
        $img->url     = $value['file'];
        $img->format  = $value['format']; 
        $img->price   = floatval($value['price']);
        $img->qty     = intval($value['quantity']);
        $prix_total   = $prix_total + $img->price;

        
        if ($img->format == "13x18") {
            $nb13x18photos = $nb13x18photos + $img->qty;
        }
        
        if (count($value['options']) == 0) {
            $img->options = "aucune";
        } else {
            $img->options = implode(',', $value['options']);
        }
        if ($img->qty > 0) {
            $ret .= print_image($img);
        }
        $current_id = $current_id + 1;
    }

    return $ret;
}

function print_field($label, $id, $fieldValidation, $textarea=false) {

    $ret =  " 
<div class='row'>
  <div class='col-4'>
    <label for='$id'>$label (*) :</label>
  </div>
  <div class='col-8'>";

         $class="";
         if ($fieldValidation->success) {
             $class="valid";
         }

         if ($fieldValidation->error) {
             $class="invalid";
         }

         $type ="text";
         if ($id == 'email') {
             $type = "email";
         } else {
             $type = "text";
         }

         if (!$textarea) {
             $ret .= "<input type='$type' name='$id' id='$id' 
                         class='form-control is-$class' 
                         value='$fieldValidation->previous'></input>";
         } else {
             $ret .= "<textarea name='$id' id='$id' 
    class='form-control is-$class'>$fieldValidation->previous</textarea>";
         }
         
         $ret .= "<div class='$class-feedback'>
          $fieldValidation->message
        </div>";
         
        $ret .="
    </div>
  </div>
";
        return $ret;
}

function print_form(SendFormValidation $validation) {
    global $prix_total, $nb13x18photos;

    $alert = '';

    // Is the number of 13x18 photos odd (FR=impair)
    /*    if ($nb13x18photos%2) {
        $alert = "<div class='alert alert-danger' role='alert'>
Vous avez un nombre impair de photos au format 13x18. Ces photos sont
imprimées et donc vendues par deux.<br>
Vous pouvez donc choisir une nouvelle au format 13x18 gratuitement.
</div>";
} */
    
    $ret = '';
    $ret .= "<h2 id='sendform'>Informations</h2>
<ul>
  <li>Suite a votre commande, vous serez contactés très prochainement par 
    mail ou télephone pour confirmer et régler votre commande.</li>
  <li><b>Tous les champs sont obligatoires</b>.</li>
  <li>Une copie de la commande vous sera envoyé par mail.</li>
</ul>
$alert
<b>Prix total</b> : $prix_total € (<b>Frais de port non inclus</b>)
<form action='index.php#sendform' method='post'>";

        $ret .= print_field("Nom", "name", $validation->nameFVS);
        $ret .= print_field("Email", "email", $validation->emailFVS);
        $ret .= print_field("Adresse postale", "mail", $validation->mailFVS,
                            true);
        $ret .= print_field("Télephone", "tel", $validation->telFVS);
  $ret .="<div class='row align-items-center'>
    <div class='col-6 col-md-4'></div>

    <div class='col-6 col-md-4'>
      <center>
        <input class='btn btn-outline-success my-2 my-sm-0' type='submit' 
           name='sendMail' value='Envoi' />
      </center>
    </div>
  <div class='col-6 col-md-4'></div>
  </div>
</form>
    
</div>";

  if (! $_COOKIE["photos"]) {
      return "<div class='alert alert-danger' role='alert'>
Votre panier est vide. </br>
Veuillez ajouter des photos.
</div>";
  } else {
      return $ret;
  }
}
   
function print_template($content) {
    global $title, $brand, $hostname, $brand;
    $tpl = new Template();
    print $tpl->render('panier', array(
        'TITLE'    => $title,
        'BRAND'    => $brand,
        'CONTENT'  => $content,
        'HOSTNAME' => $hostname,
        'BREAD'    => handle_breadcrumb()
    ));
}

/** Print the photos cookie to be sent by mail both for consummer and admin
  *
  */
function photos_cookie_mail() {
    global $cfg, $prix_total;
    
    $ret = "";
    $photos_cookie = $_COOKIE["photos"];
    $photo_arr = json_decode($photos_cookie, true);

    $gurl = $cfg['gallery_url'];
    $turl = $cfg['thumbs_url'];

    $current_id = 0;
    foreach ($photo_arr as $value) {
        $thumb_file = str_replace($gurl, $turl, $value['file']);
        $img = new Image();
        $img->id      = $current_id;
        $img->url     = $thumb_file;
        $img->format  = $value['format']; 
        $img->price   = floatval($value['price']);
        $img->qty     = intval($value['quantity']);
        $prix_total   = $prix_total + $img->price;
        if (count($value['options']) == 0) {
            $img->options = "aucune";
        } else {
            $img->options = implode(',', $value['options']);
        }
        if ($img->qty > 0) {
            $ret .= print_image_mail($img);
        }
        $current_id = $current_id + 1;
    }

    return $ret;
}

function print_image_mail(Image $img) {
    $fn = basename($img->url);
    
    return "
<tr>
  <td>
    <img src='$img->url'></img>
  </td>
  <td>
              $fn<br>
              Format : $img->format<br>
              Quantité : $img->qty<br>
              Prix : $img->price&nbsp;€<br>
  </td>
</tr>";

}

function print_success_template($mail) {
    global $title, $brand, $hostname, $brand;
    $tpl = new Template();
    print $tpl->render('success', array(
        'TITLE'    => $title,
        'BRAND'    => $brand,
        'MAIL'     => $mail,
        'HOSTNAME' => $hostname,
        'BREAD'    => handle_breadcrumb()
    ));
}

function bread_link($label, $url) {
    return "<li class='breadcrumb-item'><a href='$url'>$label</a></li>";
}

function handle_breadcrumb() {
    global $event, $subevent, $crc, $c;

    $links = bread_link($event, link_to('../?id=' . $crc));

    if ($subevent) {
        $links .= bread_link($subevent, link_to('../?id=' . $crc."&c=" . $c));
    }
    
    return "<nav aria-label='breadcrumb'>
    <ol class='breadcrumb'>
      $links
      <li class='breadcrumb-item active'>Panier</li>
    </ol>
  </nav>";
}



?>
