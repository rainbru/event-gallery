<?php

require_once(dirname(__FILE__).'/../../GetParam.php');
require_once(dirname(__FILE__).'/../../DbFile.php');

require_once(dirname(__FILE__).'/../GetHelper.php');

/** A simple, deactivable echo replacement */
function lecho($message) {
    //    echo($message);
}

/** The main API call used in partial load feature */
$cfg = parse_ini_file ('../../eg.ini');
if ($cfg['api_force_https'] == 'true') {
    // Only serve HTTPS requests 
    if($_SERVER["HTTPS"] != "on")
    {
        $res = new ApiResponse;
        $res->setStatus([403, "Forbidden.", "API will only serve on HTTPS."]);
        $res->print();
        exit();
    }
}

/** Partial load is deactivated so the API is
 *  from https://stackoverflow.com/a/57709371
 */
$pl_enable = $cfg['pl_enable']|false;
if (!$pl_enable) {
        $res = new ApiResponse;
        $res->setStatus([403, "Forbidden.", "API is deactivated."]);
        $res->print();
        exit();
}


$crc=get_param('crc'); // The gallery CRC hashsum
$c=get_param('c');     // Gallery subdir
$pid=get_param('pid'); // // The last loaded image offset (index)
// The number of image to be loaded + default value
$nb=get_param('nb') | $cfg['pl_newload'] |20;
if ($nb > 100) $nb = 100;

lecho("nb=$nb\n");
lecho("pid=$pid");
if ($crc == null || $pid==null || $c==null) {
    $nullVars = array();
    if ($crc==null) array_push($nullVars, 'crc');
    if ($pid==null) array_push($nullVars, 'pid');
    if ($c==null) array_push($nullVars, 'c');
    $nullVarsStr = implode(', ',$nullVars);
    $res = new ApiResponse;
    $res->setStatus([422, "Unprocessable Entity.",
                     "Missing parameters ($nullVarsStr)."]);
    $res->print();
    exit(0);
}

$df = new DbFile();
$gp = $df->getGalleryPath($crc);
lecho("<p>GP=".$gp);
// $gp is the path to the gallery from local mountpoint (/home/...)
// Change it to a public URL thing (https://)
$gdir = $cfg['gallery_dir'];
$gurl = $cfg['gallery_url'];
lecho("<p>GDIR=$gdir and GURL=$gurl");
lecho($gurl);

$path = str_replace($gdir, $gurl, $gp);
if ($gp == null) {
    $res = new ApiResponse;
    $res->setStatus([422, "Unprocessable Entity.",
                     "Can't get gallery path from crc '$crc'"]);
    $res->print();
    exit(0);
}

$gpath = $gp . '/' . urldecode($c);
lecho("<p>PATH=$path</p>");
$res = new ApiResponse;
$res->addDirectory($gpath, $nb);
$turl = $cfg['thumbs_url'];
$tpath =  str_replace($gdir, $turl, $gp) . '/' . urldecode($c);
$res->setPaths($tpath, $path. '/' . urldecode($c));
//
$res->setStatus([200, "OK", ""]);
$res->print();

?>
