<?php

/** The API response
 *
 * It shoudl return a 
 *  [status, content] array.
 *
 */
class ApiResponse {
    /** The error status of the request (as in HTTP server response) 
      * 200=success
      */
    protected $statusCode;
    /** The text associated with the $status code */
    protected $statusText;
    /** More text to understand the error */
    protected $statusComment;

    /** Filenames aray */
    protected $content = array();

    public $fullpath;  // The path for all full-sized photos
    public $thumbpath; // The path for thumbnails
    
    public function print() {
        echo json_encode(
            ["status" => ["status_code"    => $this->statusCode,
                          "status_text"    => $this->statusText,
                          "status_comment" => $this->status_comment],
             "answer" => ["fullpath"    => $this->fullpath,
                          "thumbpath"    => $this->thumbpath,
                          "content" => $this->content]]);
    }

    public function addFile($file) {
        array_push($this->content, $file);
    }

    /** Add images from the given directory
      *
      */
    public function addDirectory($path, $nb, $offset = -1) {
        $this->fullpath = $path;
        $loadedoffset = 0;

        $oh = new OffsetHandler($offset);
        
        
        $file_ary = scandir($path);
        if (!$file_ary) {
            echo "Can't read '$path'. scandir() returned FALSE.";
        }
        $nbloaded = 1;
        //        echo("Adding $nb images from directory $path : \n");
        //        print_r($file_ary);
        foreach ($file_ary as $file) { 
            // Remove in one test '.', '..' and all hidden files
            if ($file[0] == '.') {
                continue; 
            } 

            if ($oh->getOffset() == -1){
                // Always added
                $this->addFile($file);
            } else {
                if ($loadedoffset < $oh->getOffset()){
                    $loadedoffset += 1;
                }else {
                    $loadedoffset += 1;
                    // Only added if in offset
                    $this->addFile($file);
                }
            }
            if ($nbloaded < $nb) {
                $nbloaded += 1;
            } else {
                break;
            }
        } 
    }

    public function setStatus($st) {
        $this->statusCode     = $st[0];
        $this->statusText     = $st[1];
        $this->status_comment = $st[2];
    }

    public function contentLength() {
        return count($this->content);
    }

    public function getContent(){
        return $this->content;
    }

    /** Set the two paths used to populate the AI response */
    public function setPaths($thumbpath, $fullpath){
        $this->thumbpath = $thumbpath;
        $this->fullpath  = $fullpath;
        
    }

};


/** This object is used to return a list of photos from the nth one
  *
  */
class OffsetHandler
{
    protected $offset;

    function __construct($vOffset) {
        $this->offset = $vOffset;
    }

    function getOffset() {
        return $this->offset;
    }
}

?>
