<?php

require_once('ImageHandlerBase.php');
require('ImageHandlerConvert.php');
require('ImageHandlerImagick.php');


/** Try to avoid 'Maximum execution time of 30 seconds exceeded in' issue
 *
 * if creating the thumbnail is longer than 60 seconds (the default PHP 
 * limit), try to increase this value.
 *
 */

set_time_limit(180); // 3 minutes

/** The factory returning the right image handler according to configuration 
 *  file
 *
 */
class ImageHandlerFactory {
    /** The method extracted from configuration file */
    private $method = '';

    /** The default constructor 
     *
     * \param $ini_file An alternative configuration file or the default one
     *
     */
    function __construct($ini_file = 'eg.ini') {
        $cfg = parse_ini_file ($ini_file);
        $this->method = $cfg['thumb_method'];
    }

    /** Get the configuration file-defined image handled
     *
     * Currently, one of ImageHandlerConvert and ImageHandlerImagick.
     *
     */
    function getHandler(): ImageHandlerBase {
        if ($this->method == 'convert') {
            return new ImageHandlerConvert();
        } else {
            return new ImageHandlerImagick();
        }
    }
        
}

?>
