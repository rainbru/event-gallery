<?php

/** Count the number of files that could possibly be updated
 *
 */
class StepsCounter { 

    /**
     * The directory in which the counter is launched
     */
    private $directory;

    /** The constructor with path
      * 
      * \param $directory the path where we count steps.
      *
      */
    function __construct($directory) {
        $this->directory = $directory;
    }


    /**
     * Really count the number of files
     */
    function getCount() {
        $nbfiles = 0;
        if ($handle = opendir($this->directory)) { 
            while (false !== ($file = readdir($handle))) { 
                if ($file == '.' || $file == '..') { 
                    continue; 
                } 
                $file = $this->directory.'/'.$file;
                if (is_dir($file)) { 
                    $sc = new StepsCounter($file);
                    $nbfiles = $nbfiles + $sc->getCount();
                }
                else {
                    $nbfiles = $nbfiles + 1;
                }
            } 
            closedir($handle); 
        }
        return $nbfiles;
    }
}

?>
