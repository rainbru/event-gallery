<?php

$cfg = parse_ini_file ('eg.ini');
$should_log = $cfg['log'];
$logfile = $cfg['logfile'];

/** Launch a basic test and exit if we can write the file */
function logintro() {
    global $logfile, $should_log;

    if (!$should_log)
    {
        echo "LOG is disabled.";
        return;
    }
    
    if (!is_writable($logfile)) {
        echo "'$logfile' is NOT writable. EXITING!" . PHP_EOL;
        exit();
    }
    
    _log("Logfile is '$logfile'");

}

    
/** Returns a filename or admin/filename version of basename from a full path 
  */
function get_filename($file) {
    $b = basename($file);
    if(strpos($file, 'admin/') !== false){
        return "admin/$b";
    } else{
        return $b;
    }
    return $file;
}

/** Log a message both in webpage and in a logfile */
function _log($message) {
    global $logfile, $should_log;

    if (!$should_log)
        return;
    
    # Get current date/time
    $dt = new DateTime();
    $dtstr = $dt->format('d-m-Y H:i:s');
    
    $trace = debug_backtrace();
    $file  = get_filename($trace[0]['file']);
    $line = $trace[0]['line'];
    $loc = str_pad("$file:$line", 25);
    $msg = "$dtstr $loc $message";

    echo('<pre>' . $msg . '</pre>');
    if (!file_put_contents($logfile , $msg . "\n"))
    {
        echo("Error while writting to log file '$logfile'. ".
             "Please check perms.");
        if (!is_writable($logfile)) {
            echo "'$logfile' is NOT writable." . PHP_EOL;
        }
    }
}


?>
