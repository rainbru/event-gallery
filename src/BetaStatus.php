<?php

/** Returns the current beta status according to the installation URL
  *
  */
function isBeta() {
    $url = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    return preg_match('/beta|localhost/', $url);
}

?>
