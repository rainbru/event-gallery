<?php

/** Handle a float price to pstring conversion */
function price_to_str($price) {
    return str_replace('.', ',', sprintf("%.2f €", $price));
}

?>
