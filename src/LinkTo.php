<?php

function link_to_debug($s = false) {
    if (!$s) {
        $s = $_SERVER;
    }

    //    echo "<pre>" + print_r($s) + "</pre>";
}

// From https://www.php.net/manual/fr/function.realpath.php#84012
function get_absolute_path($path) {
    $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
    $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
    $absolutes = array();
    foreach ($parts as $part) {
        if ('.' == $part) continue;
        if ('..' == $part) {
            array_pop($absolutes);
        } else {
            $absolutes[] = $part;
        }
    }
    return implode(DIRECTORY_SEPARATOR, $absolutes);
}

/** Try to return a portable link across versions/installations
  *
  * \param the link to be modified.
  * \param A possible fake SERVER param. If not, set, will default to $_SERVER.
  *
  */
function link_to($url, $s = false) {
    if (!$s) {
        $s = $_SERVER;
    }
    
    $on_https = isset($s['HTTPS']) && $s['HTTPS'] === 'on';
    $https =  $on_https ? "https://" : "http://";

    // PORT thing already present in HTTP_HOST
    /* 
    $port = 80;
    if (isset($s['SERVER_PORT'])) {
        $port = $s['SERVER_PORT'];
    }
    
    $host = "$s[HTTP_HOST]:$port";
    */
    $host = "$s[HTTP_HOST]";
    
    $uri = strtok($s['REQUEST_URI'], '?'); // Should strip off URI parameters
    //    echo "URI is now $uri (was $s[REQUEST_URI])";
    $url = get_absolute_path("$uri/$url");
    
    return $https. "$host/$url";
}

?>
