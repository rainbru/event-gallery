function showDetails(imagepath) {
    document.getElementById('imgdetails').src = imagepath;
    $('#exampleModalCenter').modal('show');
}

function addPhoto() {
    var photos = Cookies.get('photos');
    // Default value if never used
    if (photos === undefined) {
	photos = [];
    }
    else {
	photos = JSON.parse(photos);
    }

    var filename = document.getElementById('imgdetails').src;
    // Format: 1=25x30; 2=13x18 (2 per page)
    var format = $("input[name='format']:checked").val();
    var price = parseFloat($("input[name='format']:checked").attr('data'));
    console.log("Price is " + price);
    var qty = $("#qty").val();
    var options = [];
    $.each($("input[name='option']:checked"), function(){            
        console.log("Option=" + $(this).val());
	options.push($(this).val());
	price = price + parseFloat($(this).attr('data'));
    });

    var photo = {}
    photo.file = filename;
    photo.format = format;
    photo.options = options;
    photo.price = price * qty;
    photo.quantity = qty;
    
    photos.push(photo);

    var str = JSON.stringify(photos, null, 2); // spacing level = 2
    console.log(str);
    Cookies.set('photos', JSON.stringify(photos));

    // Reset quantity to 1
    $('#exampleModalCenter').modal('hide');
    $("#qty").val(1);   

}

function closeDialog() {
    $("#qty").val(1);   
}

$(document).ready(function () {
    // Enable the add button when we choose a format
    $("input[name=format]").change(function(event){
	event.preventDefault();
	console.log("Enabling add button!");
    $("#addButton").prop("disabled", false);
    });
});
