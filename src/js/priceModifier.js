var globalCRC;

function showPricesModifierDialog(crc, priceArray)
{
    globalCRC = crc;
    
    // Remove all table childs
    $("#table tbody").empty();
    
    // Add each price array element into the table
    console.log(priceArray);
    priceArray.forEach(function(e) {
	$("#table").append(productTable(e[0], e[1], e[2]));
    });

    // Show the dialog
    $('#priceModifierDialog').modal('show');
}

function productTable(format, price, comment) {
    return "<tr><td>"+format+"</td><td>"+price+"</td><td>"+comment+"</td></tr>";
}

function innerTable(format, price, comment) {
    return '["' + format + '", ' + price + ', "' + comment + '"]';
}

function tableContent() {
    var ret = '[';
    var start = true;
    $('#table > tbody > tr').each(function(index, i){
	h = innerTable(i.cells[0].innerHTML,
		       i.cells[1].innerHTML,
		       i.cells[2].innerHTML);

	// Handle first comma
	if (start) {
	    ret = ret +  h;
	    start = false;
	} else {
	    ret = ret + ',' +  h;
	}
    });
    ret = ret + "]";
    return ret;
}

/**
 * sends a request to the specified url from a form. this will change the window location.
 * @param {string} path the path to send the post request to
 * @param {object} params the paramiters to add to the url
 * @param {string} [method=post] the method to use on the form
 *
 * Example : post('/contact/', {name: 'Johnny Bravo'});
 *
 */
function post(path, params, method='post') {

  // The rest of this code assumes you are not using a library.
  // It can be made less wordy if you use one.
  const form = document.createElement('form');
  form.method = method;
  form.action = path;

  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      const hiddenField = document.createElement('input');
      hiddenField.type = 'hidden';
      hiddenField.name = key;
      hiddenField.value = params[key];

      form.appendChild(hiddenField);
    }
  }

  document.body.appendChild(form);
  form.submit();
}

$(document).ready(function() {
    $('tbody').sortable();

    $("#table").on('click', 'tr',  function(){
	// Do not select the header
	if (!$(this).hasClass('header')) {
	    $(this).addClass('selected').siblings().removeClass('selected');    
	    var value=$(this).find('td:first').html();

	    // Enable disabled buttons
	    $("#priceModify").prop('disabled', false);
	    $("#priceDelete").prop('disabled', false);
	}
    });

    $("#priceAdd").click(function(){
	var format = $("#format").val();
	// The following replace is used to handle both . and , as
	// decimal separators
	var price = parseFloat($("#price").val().replace(',', '.'));
	var comment = $("#comment").val();
	if (format.includes("\"") || comment.includes("\"")) {
	    alert("Les champs ne peuvent contenir le caractère \". Préférez "+
		  "' ou « et ». (Alt Gr + W|X).");
	} else {
	    if (price && format) {
		$("#table").append(productTable(format, price, comment));
	    } else {
		alert("Format et Prix sont obligatoires.");
	    }
	}
    })

    $("#priceDelete").click(function(){
	$('tr.selected').remove();

	// Enable disabled buttons
	$("#priceModify").prop('disabled', true);
	$("#priceDelete").prop('disabled', true);

    })    

    $("#priceModify").click(function(){
	var i = $('tr.selected');
	// Copy element content
	$('#format').val(i.find("td").eq(0).html());
	$('#price').val(i.find("td").eq(1).html());
	$('#comment').val(i.find("td").eq(2).html());
	
	$('tr.selected').remove();

	// Enable disabled buttons
	$("#priceModify").prop('disabled', true);
	$("#priceDelete").prop('disabled', true);
    })

    $("#save").click(function(){
	tc = tableContent();
	post('changePrices.php', {crc: globalCRC, prices: tc});
    });

    $("#cancel").click(function(){
	$('#priceModifierDialog').modal('hide');
    });

});

