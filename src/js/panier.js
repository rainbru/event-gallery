/** The file included from panier.tmpl */

function removeTrailingSlash(site)     
{
    site = site.split("#")[0]; // Remove anchor if exists
    return site.replace(/\/$/, "");
} 

/** photoId is the id ioin the cookies array */
function removeOne(photoid) {

    console.log(photoid);
    var photos = JSON.parse(Cookies.get('photos'));


    // remove One
    var pricePerPhoto = photos[photoid].price / photos[photoid].quantity;
    var newQty = photos[photoid].quantity - 1
    photos[photoid].quantity = newQty;
    photos[photoid].price = newQty * pricePerPhoto;

    Cookies.set('photos', JSON.stringify(photos));
    
    console.log(photos[photoid]);
    var newUrl = removeTrailingSlash(window.location.href) +'#photo-' + photoid;
    console.log("anchor = " + newUrl);

    window.location = newUrl;
}
