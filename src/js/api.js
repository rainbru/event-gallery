//Last offset
var lastOffset=0; 
var initialLoad=0;  // The initial numebr of image
var newload=0;
var threshold=0;

var loadingFlag=false;

var gallery_crc = "";
var gallery_c = "";

// From https://stackoverflow.com/a/52560829
var hlpr = {
        lastExec: new Date().getTime(),
    isThrottled: function (timeout) {
	var diff = (new Date().getTime() - this.lastExec);
        if (diff < timeout) {
            return false;
        }
        this.lastExec = new Date().getTime();
        return true;
    }
};

// Just a console.log call we can globally comment out
function l(msg) {
    console.log(msg);
}

/** Set both the loadingFlag and the GIF animation */
function setLoading(bool) {
    loadingFlag = bool;
    if (loadingFlag) {
	$('#loading').show();
    } else {
	$('#loading').hide();
    }
}

/** Set the global threshold when we start to call API */
function setThreshold(pl_threshold)
{
    threshold = pl_threshold;
}

/** Return an error status to a readable string */
function responseToStr(JSONobj) {
    return "Error: " + JSONobj.status.status_code +
	", " + JSONobj.status.status_text + "\n\n" +
	JSONobj.status.status_comment;
}


// Add the thumbnail to the webpage given a fullpath
// File: The file name (stripped from paths)
// Return: true if thumb added
function addThumb(file, thumbpath, fullpath) {
    let lfi = document.getElementById(file);
    l("LFI => " + lfi);
    if (lfi == null) {
	thumb="<div class='ih-item'><a onclick='javascript:showDetails(\"" +
	    fullpath + "\");'><div class='img'><img src='" +
	    thumbpath + "' id='" + file + "'></img></a></div></div>";
	$('.container').append(thumb);
	lastOffset += 1;
	return true;
    } else {
	return false;
    }
}

// Params :
// crc: global crc : used to store current gallery hashsum
// gc: global actual gallery subdir name
// nb: The number of photos to be load
function APIcall(gcrc, gc, nb) {
    let data = { crc: gcrc,
		 c: gc,
		 pid: lastOffset,
		 nb: nb };
    l("Calling ajax : data=" + JSON.stringify(data));

    
    $.ajax({
	url: "api/photo/get.php",
	type: "get",
	data: {
	    crc: gcrc,
	    c: gc,
	    pid: lastOffset,
	    nb: nb
	}
    })
	.done(function(data) {
	    l("Received reponse : " + data);
	    const obj = JSON.parse(data);
	    const sc = obj.status.status_code;
	    l("Received reponse Length : " + obj.answer.content.length);
	    if (sc >=200 && sc < 300) {
		loaded = 0
		// alert(obj.answer.path);
		files = obj.answer.content
		files.forEach(function(file) {
		    thumbpath = obj.answer.thumbpath + '/' + file;
		    fullpath = obj.answer.fullpath + '/' + file;
		    if (addThumb(file, thumbpath, fullpath)) {
			loaded += 1;
			l("loaded = " + loaded);
			//if (loaded > newLoad){ break; }
		    }
		});
		setLoading(false);
	    } else {
		alert(responseToStr(obj));
	    }
	})
	.fail(function() {
	    alert( "error" );
	})
}


// Helping with the AJAX call
$(document).ready(function () {

    $(window).unbind('scroll');
    
    // The scroll event
    $( window ).scroll(function() {
	// Use to prevent this to be called twice
	if (!hlpr.isThrottled(500))
	    return;
	
	var scrollPosition = window.pageYOffset;
	var windowSize     = window.innerHeight;
	var bodyHeight     = document.body.offsetHeight;
	var thre = Math.max(bodyHeight - (scrollPosition + windowSize), 0);

	if (thre < threshold) {
	   
	    l('Treshold : ' + thre + '( > ' + threshold + ')');
	    // Must call API and set loadingFlag to true
	    setLoading(true);
	    APIcall(gallery_crc, gallery_c, newload);
	    setLoading(false);
	}
    });
});


// initial: The initial number of photos loaded
function partialLoad(initial, newl, crc, c) {
    gallery_crc = crc;
    gallery_c = c;
    initialLoad = initial;
    newload=newl;
    APIcall(crc, c, initial);
}
