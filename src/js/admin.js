/** Show the actions div of a gallery row
  *
  */
function showActions(crc)
{
    var divid = 'actions-' + crc;
    var ds = document.getElementsByClassName("actionsDiv");
    // Uses Array.from as of https://stackoverflow.com/a/35970011
    // Because ds is a HTML collection which do not implement forEach()
    // function.
    Array.from(ds).forEach(function(e){
	e.style.display = "none";
    });
    
    var buttonsDiv = document.getElementById(divid);
    buttonsDiv.style.display = "block"

    // Get new prices
    /*
    var pL =  document.getElementById('price-25x30-' + crc).val;
    console.log(pL);
*/
}

/** Hide the actions div of a gallery row
  *
  */
function hideActions(divid)
{
    document.getElementById(divid).style.display = "none";
}
