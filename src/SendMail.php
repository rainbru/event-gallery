<?php

/** 
 * A basic mailer for php
 * 
 */
class SendMail { 

    private $method;    //!< The mail method from the config file
    private $recipient; // The mail recipient from the config file

    public $content;
    public $subject;
    
    /**
     * Constructor: it must keep all one-access vars to be able to
     * produce images fast.
     *
     * \param $ini_file is used to change config file location from unit tests.
     *
     */
    function __construct($ini_file = 'eg.ini') {
        $cfg = parse_ini_file ($ini_file);
        $this->method = $cfg['mail_method'];
        $this->recipient = $cfg['mail_recipient'];

        if (!$this->recipient) {
            throw new Exception("Mail recipient is empty");
        }
    }

    /** Send a mail
      *
      * \param $from the from part of the address. Kust be in the form 
      *              'Birthday Reminder <birthday@example.com>'.
      * \param $test Replace with test content if true.
      * \param $to The mail recipient
      *
      */
    function send($from, $test = false, $to = false) {

        if (!$to) {
            $to = $this->recipient;
        }
        
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        // Additional headers
        $headers .= 'To: ' . $to . "\r\n";
        $headers .= 'From: '. $from . "\r\n";

        $message = "";

        if ($test) {
            $message = $this->testContent();
            $subject = "test";
        } else {
            $message = $this->content;
            $subject = $this->subject;
        }
        
        if(!mail($to, $subject, $message, $headers)) {
            throw new Exception("Cannot send mail");
        } else {
            return true;
        }
        
    }

    /** A test content for email
      *
      * \return A HTML mail content
      *
      */
    function testContent() {
        return '
<html>
<head>
  <title>Event gallery test mail</title>
</head>
<body>
  <p>Ceci est un simple mail de test!</p>
  <table>
    <tr>
      <th>Person</th><th>Day</th><th>Month</th><th>Year</th>
    </tr>
    <tr>
      <td>Joe</td><td>3rd</td><td>August</td><td>1970</td>
    </tr>
    <tr>
      <td>Sally</td><td>17th</td><td>August</td><td>1973</td>
    </tr>
  </table>
</body>
</html>
';
    }
    
}
