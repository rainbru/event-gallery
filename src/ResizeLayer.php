<?php
/* A partial port, mainly to handle thumbs (forbidden files)

The main of the code is currently in adin/index.php file
*/


/** A class used to check for forbidden files before running R+Z
  *
  I've done a quick fix in admin/index.php:RZfile(), it's a very simple
  one and should work

  */
class ResizeLayer
{
    /** These file will create a warning if present */
    //    private forbidden_files = ['.@__thumb', '.DS_Store'];
    //    private input;
    

    /** The constructor
      *
      */
    function __construct( $input_path ){
        $this->input = $input_path;
    }
}


?>
