<?php

function truncate_text($text, $chars = 25) {
    $len = strlen($text);  
    if ($len <= $chars) {
        return $text;
    }
    $tend = substr($text,$len - 10,$len);
    $ntext = $text." ";
    $ntext = substr($text,0,10);
    $ntext = $ntext." ... ".$tend;
    return "<span title='$text'>$ntext</span>";
}

?>
