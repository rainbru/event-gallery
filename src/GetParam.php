<?php
/** Get the GET param if exists
  *
  * overwise, return null. Used both by index and panier.
  *
  */
function get_param($name) {
    if (array_key_exists($name, $_GET)) {
        return $_GET[$name];
    } else {
        return null;
    }
}
?>
