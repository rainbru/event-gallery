<?php

require_once('ImageHandlerBase.php');

/** The conert command-based thumbnail generation class
 *
 */
class ImageHandlerConvert extends ImageHandlerBase {

    /** Generate the thumbnail using convert system command
     *
     * \param $img the full path to to-be-resized image.
     *
     * \return May return an array containing command output in case of error.
     *
     */
    public function generateThumbnail($img)
    {
        if (is_file($img)) {
            // Call the convert system command
            $input = realpath($img);
            $output = $this->getThumbPath($img);

            if (strcmp($input,$output) == 0) {
                $msg="generateThumbnail() is about to override original image.";
                throw new Exception($msg);
                return;
            }
            $size = $this->thumbwidth.'x'.$this->thumbheight;
            $cmd = "convert \"$input\" -auto-orient -thumbnail $size \"$output\"";

            $cmd_output_ary = array();
            $ret_value = "";
            exec($cmd, $cmd_output_ary, $ret_value);
            if ($ret_value != 0) {
                throw new Exception("convert failed ($ret_value) when calling '$cmd'");
            }
            parent::keepThumbDate($input, $output);
        }
        else {
            throw new Exception("{$img} is not a valid image.");
        }
    }

    /** Warning, in the process of creating the thumbnail, we lost EXIF datas
      */
    public function generateThumbnailWithLayer($img, $out)
    {
        $input = '"' . realpath($img) . '" "' . $this->RZ_layer . '"';
        $size = $this->RZ_width.'x'.$this->RZ_height;
        $cmd = "convert -composite $input -auto-orient -thumbnail $size \"$out\" 2>&1";
        $cmd_output_ary = array();
        $ret_value = "";
        exec($cmd, $cmd_output_ary, $ret_value);
        if ($ret_value != 0) {
            return print_r($cmd_output_ary);
            //  throw new Exception("convert failed ($ret_value) when calling '$cmd' : $cmd_output");
        }
        parent::changeThumbDate(realpath($img), $out);

        
        
    }

}
