<?php

require(__DIR__.'/../DbFile.php');
require(__DIR__.'/../LinkTo.php');

/** Called when we change prices */

if($_SERVER['REQUEST_METHOD'] == "POST")
{
    // Change prices
    $crc = $_POST['crc'];
    $prices = $_POST['prices'];

    $df = new DbFile();
    $df->setPrices($crc, $prices);
    $df->save();

    $array_data =  json_decode($_POST['prices']);

    // Fix a redirect but. Was redirecting to $HOST/admin !
    $to = link_to('/..') . '/';
    header('Location: ' . $to);     
}
 

?>
