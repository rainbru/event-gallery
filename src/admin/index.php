<?php
include(__DIR__.'/../template.php');
require(__DIR__.'/../StepsCounter.php');
require(__DIR__.'/../DbFile.php');
require(__DIR__.'/../ImageHandlerBase.php');
require(__DIR__.'/../ImageHandlerFactory.php');
require(__DIR__.'/../SendMail.php');
require(__DIR__.'/../ExcludedPath.php');
require(__DIR__.'/../TruncateText.php');
require(__DIR__.'/../BetaStatus.php');
require(__DIR__.'/../LinkTo.php');
require(__DIR__.'/../ResizeLayer.php');
require(__DIR__.'/../Utils.php');
require(__DIR__.'/../Logger.php');

/*
include(__DIR__.'/../FatalHandler.php');
register_shutdown_function( "fatal_handler" );
*/
logintro();
_log("Loading logfile");
$i = parse_ini_file ('../eg.ini');


//link_to_debug();

# Handle the beta status
$beta = isBeta() ? 'beta' : '';

// Should we enable btnUpdateGalleries bouton ?
$enableUpdateGalleriesBtn = false;

$dir = dirname(__FILE__);
$htfile = $dir . "/.htpasswd";
$gdir = $i['gallery_dir'];
$tbl = galleries_table($gdir);
$hostname = $i['hostname'];
$brand = $i['brand'];
          
$progressvalue = 0;

// Set this falue to True to deactrivate RZ button */
$rz_warning = false;

/*
foreach ($exif as $key => $section) {
    foreach ($section as $name => $val) {
        $str= $str . "$key.$name: $val<br />\n";
    }
}

$tbl = $tbl . "<p>Données EXIF :
<p>Photo: $exifile
<p>DateTimeOriginal: $dto
<pre>$str</pre>
";
*/

/*
echo $i['thumbs_url'];
*/

$df = new DbFile();
$file = $df->getPath();
/*echo "<p>db file :<p>";
echo $file, file_exists($file) ? ' exists' : ' does not exist', "<p>";
echo $file, is_readable($file) ? ' is readable' : ' is NOT readable', "<p>";
echo $file, is_writable($file) ? ' is writable' : ' is NOT writable', "<p>";
*/
$ihf = new ImageHandlerFactory();
$ih = $ihf->getHandler();
$gtdir = $ih->getThumbPath($gdir);
// Here we'll store every warnings
$dbfile_warnings= "<p class='text-success'>NONE</p>"; 

// Test if DbFile is writable
if (!is_writable($file)) {
    $dbfile_warnings="<p class='text-warning'>- The '$file' file isn't writable</p></br>";
}


$pmTpl = new Template();
$pm = $pmTpl->render('dialog/priceModifier', array());

// Print template
$tpl = new Template();
print $tpl->render('admin', array(
    'BETA'            => $beta,
    'DIR'             => $dir,
    'HTFILE'          => $htfile,
    'GDIR'            => $gdir,
    'TBL'             => $tbl,
    'MMETHO'          => $i['mail_method'],
    'MRECIP'          => $i['mail_recipient'],
    'DBFILE'          => $df->getPath(),
    'DBFILE_WARNINGS' => $dbfile_warnings,
    'RZ_IMAGEMAGICK'  => get_imagemagick(),
    'RZ_CONVERT'      => get_convert(),
    'CFG_FORMATS'     => get_formats_string(),
    'CFG_OPTIONS'     => get_options_string(),
    'CFG__RZ_INPUT'   => get_rz_input(),
    'CFG__RZ_OUTPUT'  => get_rz_output(),
    'CFG__RZ_SIZE'    => $i['RZ_width']."x".$i['RZ_height'],
    'CFG__RZ_LAYER'   => get_rz_layer(),
    'CFG__RZ_FILES'   => get_rz_nbfiles(),
    'THUMBS_DIR'      => get_thumbs_dir(),
    'THUMBS_URL'      => get_thumbs_url(),
    'HTACCESS_LIST'   => htaccess_list(),
    'THUMB_METHOD'    => get_thumb_method(),
    'BRAND'           => $brand,
    'HOSTNAME'        => $hostname,
    'PRICE_MODIFIER'  => $pm,
    'LOG_STATUS'      => log_status()
));

if ($enableUpdateGalleriesBtn) {
    activate_button("btnUpdateGalleries");
}


// If a RZ warninng occured, deactivate the button
if ($rz_warning) {
    rz_deactivate_button();
}

// Test if the gallery dir is a directory
if (!is_dir($gdir)) {
    echo("Le repertoire des galleries est illisible!");
}

if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['launchRZ'])) {
    launchRZ();
}


// Handle mail test
if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['testMail']))
    {
        $mailTo = $_POST['testMailName'];
        appendToStory("Sending mail test to $mailTo");
        try {
            $sm = new SendMail();
            $sm->send("Admin test", true, $mailTo);
            appendToStory("Mail correctly sent");
        } catch (Exception $e) {
            appendToStory("Cannot send mail :( : " . $e->getMessage());
        }
    }

// Handle Generation
if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['someAction']))
    {
        // do stuff
        appendToStory("Actually indexing galleries...");

        // First count all files
        $filecount = 0;
        
        // Getting non-indexed galleries
        $excluded_paths = new ExcludedPath();
        $df = new DbFile();
        $ihf = new ImageHandlerFactory();
        $ih = $ihf->getHandler();
        appendToStory("Opening dir $gdir");
        if ($handle = opendir($gdir)) { 
            while (false !== ($file = readdir($handle))) { 
                if ($file == '.' || $file == '..') { 
                    continue; 
                } 
                $file = $gdir.$file;

                if ($excluded_paths->isExcluded($file)) {
                    // If dir is excluded, simply continue to next dir
                    appendToStory("'$file' is excluded");
                    continue;
                }
                appendToStory("Testing dir $file");
                if (is_dir($file)) {
                    if (!$df->isIndexed($file)) {
                        // Add this non index files to filecount
                        $sc = new StepsCounter($file);
                        $filecount = $filecount + $sc->getCount();
                        setProgressMax($filecount);

                        $tobeadded = $file;

                        // Subdirs
                        if ($handle2 = opendir($file)) { 
                            while (false !== ($subdir2 = readdir($handle2))) {
                                if ($subdir2 == '.' || $subdir2 == '..') { 
                                    continue; 
                                } 
                                $photodir = $tobeadded.'/'.$subdir2;

                                $tbc = $ih->getThumbPath($photodir);
                                appendToStory("~~> Creating dir '$tbc' ...");
                                $ih->createThumbDir($tbc);
                                
                                // TODO: handle all files in directory
                                if ($handle3 = opendir($photodir)) {
                                    while (false !== ($subdir3 = readdir($handle3))) {
                                        if ($subdir3 == '.' || $subdir3 == '..') { 
                                            continue; 
                                        }
                                        $photo = $photodir.'/'.$subdir3;
                                        appendToStory("~~> Handling '$photo' ...");
                                        try {
                                            $ih->generateThumbnail($photo);
                                            step();
                                        } catch (Exception $e){
                                            appendToStory("~~> Error handling '$photo' : ". $e->getMessage());

                                        }
                                        
                                    }

                                }
                                
                            }

                        }
                        $df->add("$tobeadded");
                        $savestate = $df->save();
                        if ($savestate) {
                            appendToStory("Append '$tobeadded' to DbFile");
                        }
                        else {
                            appendToStory("Failed to save DbFile");
                        }
                    }
                }
            } 
            closedir($handle); 
        }
        
        

        if(empty($_SESSION['i'])){
            $_SESSION['i'] = 0;
    
            $total = 10;
            for($i=$_SESSION['i'];$i<$total;$i++)
                {
                    
                    $_SESSION['i'] = $i;
                    $percent = intval($i/$total * 100)."%";   
                    
                    step();
                    echo '<script>
    parent.document.getElementById("information").innerHTML="'.$percent.' is processed.";

    </script>
    ';
                    ob_flush(); 
                    flush(); 

                }
            completed();
            ob_flush(); 
            flush(); 
            deactivate_button("btnUpdateGalleries");
    }
}

function appendToStory($text) {
    echo '<script>
    parent.document.getElementById("story").value = parent.document.getElementById("story").value + "'.  $text . '\n";
    </script>
    ';
            
    ob_flush(); 
    flush(); 
}

function completed() {
    echo '<script>parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Process completed</div>"</script>';
    ob_flush(); 
    flush(); 
}

/** Returns the subdirs part of a gallery table
  *
  * \param $dir The parent directory.
  *
  */
function subdirs_table($dir) {
    $subdirs = "";

    $files = scandir($dir);
    foreach ($files as $f) {
        $file = $f;
        if ($file == '.' || $file == '..') { 
            continue; 
        } 
        $subdirname= $file;
        $file = $dir.'/'.$file;
        if (is_dir($file)) {
            $subdirs = $subdirs . "<tr class='child'>
                                      <td colspan='8'>$subdirname</td>
                                  </tr>";
        }
    }
    return $subdirs;
}

/** Returns the HTML of a single gallerie
 *
 */
function gal_to_html($dir, $path) {
    global $enableUpdateGalleriesBtn, $i;
    
    $excluded_paths = new ExcludedPath();

    $db = new DbFile();
    $crc = $db->getPathHashSum($path);

    $sc = new StepsCounter($path);
    $count = $sc->getCount();

    $subdirs = subdirs_table($path);
    $indexed = "";
    if ($excluded_paths->isExcluded($path)){
            return "
<tr>
  <td>$dir</td>
  <td>---</td>
  <td>---</td>
  <td>---</td>
  <td>---</td>
  <td>---</td>


  <td>---</td>
  <td>Exclus</td>
</tr>    
";

    }

    $actionsdivid = 'actions-'.$crc;
    // General options for two input elements
    $genopts = "type='number' min='0' max='2000' step='0.01'";
    
    if ($db->isIndexed($path)) {
        // Placeholders or values
        // TODO: if the price is individualized for this galleru, just use :
        $indexed = "Indéxée";
        //        $link = "<a href='/event-gallery/?id=$crc' target='_blank'>$crc</a>";
        $link = "<a href=".link_to("../?id=$crc")." target='_blank'>$crc</a>";

        /*        $form = "<form action='index.php' method='post' autocomplete='off'>
    <td><input $genopts name='20x30price' id='price-20x30-$crc' size='5' 
           onfocus='showActions(\"$crc\");' $phL></td>
    <td><input $genopts name='13x18price' id='price-13x18-$crc' size='5' 
           onfocus='showActions(\"$crc\");' $phS></td>
    <td>
      <input type='hidden' name='galPath' value='$path'></input>
      <div id='$actionsdivid' class='actionsDiv'>
        <button type='submit' class='btn btn-success' name='saveNewPrice'>
          <i class='fa fa-check'></i>
        </button>
        <button type='submit' class='btn btn-danger' name='cancelNewPrice'>
          <i class='fa fa-times'></i>
        </button>
      </div>
    </td>
  </form>"; */
        // Should come from DbFile!!
        $gpath  = $db->getGalleryPath($crc);
        $prices = $db->getPrices($gpath);
        $arr = $i['default_formats'];
        $pricesClass="";
        if ($prices) {
            $arr = $prices;
        } else {
            $prices = $arr;
            $pricesClass="class='default'";
        }
        $options=pricesToOptions($arr);
        $form="<td><form>
    <select $pricesClass name='pets' id='price-$crc' multiple='multiple'>
      $options
    </select></form><button type='button' class='btn btn-info modifyPriceButton' name='saveNewPrice'
onclick='javascript:showPricesModifierDialog(\"$crc\", $arr);'>
          Modifier
        </button></td>";
        
    }
    else {
        $indexed = "Non indéxée";
        $link = $crc;
        $form="<td colspan='3'>
<div class='alert alert-warning' role='alert'>
Impossible de modifier les prix<br>
 d'une gallerie non indéxée.
</div></td>";
        $enableUpdateGalleriesBtn = true;
    }

    // onfocus='showActions(\"$actionsdivid\")'

    $path = truncate_text($path, 50);
    return "
<tr>
  <td>$dir</td>
  <td>$path</td>
  <td>$count photos</td>
  $form
  <td>$link</td>
  <td>$indexed</td>

</tr>    

$subdirs
";
}

// onfocusout='hideActions(\"$actionsdivid\");'
// onfocusout='hideActions(\"$actionsdivid\");'

/** Return all known galleries in a HTLML table
 *
 */
function galleries_table($dir) {
    $ttl = "Les prix par defaut sont de couleur bleu/gris. Si aucun prix n`apparait, il peut y avoir une erreur dans le fichier gallery.db.";
    $o="<table>
  <tr>
    <th colspan='3'>Gallerie</th>
    <th title='$ttl'>Prix <span class='badge'>*</span></th>
    <th>URL</th>
    <th>Statut</th>
  </tr>";

    $nbfiles = 0;
    
    if ($handle = opendir($dir)) { 
        while (false !== ($file = readdir($handle))) { 
            if ($file == '.' || $file == '..') { 
                continue; 
            } 
            $galname = $file;
            $file = $dir.$file;
            if (is_dir($file)) {
                $o = $o . gal_to_html($galname, $file);
                $sc = new StepsCounter($file);
                $nbfiles = $nbfiles + $sc->getCount();

            }
            else {
                $nbfiles = $nbfiles + 1;
            }
        } 
        closedir($handle); 
    }
    
    return $o . "</table>";
}

function step() {
    global $progressvalue;
    global $filecount;
    $progressvalue = $progressvalue + 1;
    appendToStory("progress is now $progressvalue/$filecount");

    echo "<script>parent.document.getElementById('prog').value=$progressvalue;</script>";
    ob_flush(); 
    flush(); 
}

function setProgressMax($max) {
    echo "<script>parent.document.getElementById('prog').max=$max;</script>";
    ob_flush(); 
    flush(); 
}

function get_json_string($str) {
    $fmt = json_decode($str, true);
    
    $out = "<table><tr>
<th>Label</th>
<th>Prix</th>
<th>Commentaires</th>
</tr>";
    if (!$fmt) {
        $out = $out . "Formats was NULL</table>";
        return $out;
    }
    
    foreach ($fmt as $item) {
        if ($item) {
            $out = $out . "<tr>";
            
            foreach ($item as $key => $value) {
                $out = $out . "<td>$value</td>";
            }
            $out = $out . "</tr>";

        }
    }
    $out = $out . "</table>";
    return $out; 
}

function get_formats_string() {
    global $i;
    return get_json_string($i['default_formats']);
}

function get_options_string() {
    global $i;
    return get_json_string($i['default_options']);
}

// Resize+layer specificcode
function get_rz_input() {
    global $i, $rz_warning;
    $path = $i['RZ_input'];
    $class = "";
    $text = "";
        
    if (file_exists($path)) {
        $class="text-success";
        $text="répertoire trouvé";
    }
    else {
        $class="text-warning"; 
        $text="répertoire introuvable";
        $rz_warning = true;
    }
    return "<tt>$path</tt>&nbsp;(<span class='$class'>$text</span>)";
}

function get_rz_output() {
    global $i, $rz_warning;
    $path = $i['RZ_output'];
    $class = "";
    $text = "";
        
    if (file_exists($path)) {
        $class="text-warning"; 
        $text="répertoire existant";
        $rz_warning = true;
    }
    else {
        $class="text-success";
        $text="répertoire inexistant";

        // test parent dir is writable
        $parent_path = dirname($path);
        if (!is_writable(dirname($path))) {
            $class="text-warning"; 
            $text="répertoire parent ($parent_path) n'est pas modifiable";
            $rz_warning = true;
        }
        
    }
    return "<tt>$path</tt>&nbsp;(<span class='$class'>$text</span>)";
}

function get_rz_layer()
{
    global $i, $rz_warning;
    $path = $i['RZ_layer'];
    $class = "";
    $text = "";
        
    if (file_exists($path)) {
        $class="text-success";
        $text="fichier trouvé";
    }
    else {
        $class="text-warning"; 
        $text="fichier introuvable";
        $rz_warning = true;
    }
    return "<tt>$path</tt>&nbsp;(<span class='$class'>$text</span>)";
}

// Resize+layer specificcode
function get_rz_nbfiles()
{
    global $i;
    $path = $i['RZ_input'];
    $class = "";
    $text = "";
        
    if (!file_exists($path)) {
        return 0;
    }

    $sc = new StepsCounter($path);
    return $sc->getCount();

}


function rz_deactivate_button()
{
    deactivate_button("rzbtn");
}

function get_imagemagick() {
    $class = '';
    $text = "";
    
    if (!extension_loaded('imagick')) {
        $class="text-warning"; 
        $text="NON (extension_loaded() failed)";
        $rz_warning = true;

        if (class_exists("Imagick")) {
            $text .= "(class_exists() succeed)";
        } else {
            $text .= "(class_exists() failed)";
        }

    }
    else {
        $class="text-success";
        $text="OUI";

    }
    return "<span class='$class'>$text</span>";

}

/** The image resizer+layout function
  *
  */
function launchRZ() {
    global $i;
    $input = $i['RZ_input'];
    $output = $i['RZ_output'];
    $width = intval($i['RZ_width']);
    $height = intval($i['RZ_height']);
    $quality = $i['RZ_quality'];
    disable_UpdateGalleries_button();
    rz_deactivate_button();
    appendToStory("Launching resize+layout");
    $sc = new StepsCounter($input);
    setProgressMax($sc->getCount());

    $ih = (new ImageHandlerFactory())->getHandler();
    RZfile( new DirectoryIterator( $input ), $ih);


}

/** Resize and add layer to one file (or a dir) *
 *
 * DO NOT FORGET TO update recursive call!
 *
 */
function RZfile( DirectoryIterator $dir, ImageHandlerBase $handler) {
    //, $input, $output, $width, $height, $quality, $layer ) {
    global $i;
    foreach ( $dir as $node ) {
        if ( $node->isDir() && !$node->isDot() )
            {
                // Test if path doesn't exist
                $out = $handler->getRzToOutputPath($node->getPathname());
                if (!is_dir($out)) {
                    appendToStory("Création du repertoire '$out'");
                    mkdir($out, 0777, true);
                }

                // Do not forget to update this line if you're adding
                // parameter to ghis function
                RZfile(new DirectoryIterator($node->getPathname()), $handler );

            }
        else if ( $node->isFile() ) {
            $bn = $node->getBasename();
            if ($bn == '.@__thumb' || $bn == '.DS_Store') {
                continue;
            }
            
            $outpath = $handler->getRzToOutputPath($node->getPathname());
            appendToStory("- Génération de  $outpath");
            
            // Call to ImageHandler->generateThumbnailWithLayer
            $output_ary = $handler->generateThumbnailWithLayer
                        ($node->getPathname(), $outpath);
            //  appendToStory(print_r($output_ary));
            step();
        }
    }
    //    return $data;
}

/** Deactivate the *Update Galleries* button */
function disable_UpdateGalleries_button()
{
    deactivate_button("btnUpdateGalleries");
}

function get_thumbs_dir()
{
    global $i;
    $dir =  $i['thumbs_dir'];
    $ret = $dir . "&nbsp(&nbsp;";

    if (file_exists($dir)) {
        $ret.= "<span class='text-success'>Exists</span>";
    } else {
        $ret.= "<span class='text-warning'>Does not exists</span>";

    }

    $ret.= ",&nbsp";

    if (is_readable($dir)) {
        $ret.= "<span class='text-success'>Readable</span>";
    } else {
        $ret.= "<span class='text-warning'>Not readable</span>";
    }

    $ret.= "&nbsp)";
    return $ret;
}

function get_thumbs_url()
{
    global $i;
    $dir =  $i['thumbs_url'];
    return "<a href='$dir'>$dir</a> (you must test manually)";
}

function htaccess_test($dir) {
    $file = $dir . '.htaccess';
    $found = file_exists($file) ? 'Oui' : ' Non';
    return "
<tr>
  <td>$file</td>
  <td>$found</td>
</tr>";
}

function htaccess_list() {
    global $i;
    $ret  = htaccess_test($i['gallery_dir']);
    $ret .= htaccess_test($i['thumbs_dir']);
    return $ret;
}

/** Test if we can find the `convert` command */
function get_convert() {
    //Try to get ImageMagick "convert" program version number.
    //Print the return code: 0 if OK, nonzero if error. 
    exec("convert -version", $out, $rcode);

    if ($rcode == 0) {
        $class="text-success";
        $text="OUI";
    } else {
        $class="text-warning";
        $text="NON";
    }

    return "<span class='$class'>$text</span>";
}

function get_thumb_method() {
   global $i;
   return $i['thumb_method'];
}

/** Deactivate a HTML button
  * 
  * \param $id the HTML id of the button.
  */
function deactivate_button($id) {
    echo "<script>    parent.document.getElementById('$id').disabled = true
    </script>";
    ob_flush(); 
    flush(); 
}

function activate_button($id) {
    echo "<script>    parent.document.getElementById('$id').disabled = false
    </script>";
    ob_flush(); 
    flush(); 
}

/** Returns the given json string as a HTML options list
  *
  * May throw an exception if $prices can't be decoded to json.
  *
  */
function pricesToOptions($prices) {
    $pricesArray =  json_decode($prices);
    if ($pricesArray == NULL) {
        $msg = "Failed to decode default_format to JSON. 
                  Prices table will be empty.";
        echo("<br><b>$msg</b></br>");
        /* Next line will simply stop processing. We prefer the program
           continue and create an empty prices table.
         */
        //        throw new Exception("$msg");
    }
    
    $ret='';
    foreach ($pricesArray as $price) {
        $pstr = price_to_str($price[1]);
        $ret = $ret . "<option>$price[0] : $pstr</option>";
    }
    return $ret;
}

/** Returns the string used in the admin card on the log file */
function log_status()
{
    global $i;

    // Activé ?
    $active = "<tr><td>Fonction active</td><td>";
    if ($i['log']) {
        $active .= "Oui";
    } else {
        $active .= "Non";
    }
    $active .= "</tr>";

    // Logfile
    $lf = $i['logfile'];
    $active.= "<tr><td>Fichier de log</td><td>$lf</td></tr>";

    // Wan write
    $iw = is_writable($i['logfile']) ? "Oui" : "Non";
    $active.= "<tr><td>Accès en écriture</td><td>$iw</td></tr>";

    return $active;
}

?>


