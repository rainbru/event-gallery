<?php
require_once(dirname(__FILE__).'/../DbFile.php');

use PHPUnit\Framework\TestCase;


/**
 *
 * Do not forget to call DbFile constructor with the *true* parameters
 * to use the test file.
 */

final class DbFileTest extends TestCase
{

    /**
     * Just to test the DbPath isn't empty
     */
    public function testDbPath()
    {
        $df = new DbFile(true);
        $this->assertNotEmpty( $df->getPath());
    }

    /** 
     * Test that we can get a valid Hashsum from a hash
     *
     */
    public function testPathHashSum()
    {
        $df = new DbFile(true);
        $this->assertNotEmpty( $df->getPathHashSum($df->getPath()));
    }


    public function testSizeEmpty() {
        $df = new DbFile(true);
        $this->assertEquals( $df->size(), 1);
    }

    public function testSizeAdd() {
        $df = new DbFile(true);
        $size = $df->size();
        $df->add("/example/path");
        $df->save();
        $this->assertEquals( $df->size(), $size);
    }

    /** DbFile must have a save function
     *
     */
    public function testSave() {
        $df = new DbFile(true);
        $df->save();
        $this->assertTrue(TRUE);
    }

    /** DbFile must save the added paths
     *
     */
    public function testSaveAdd() {
        $df = new DbFile(true);
        $df->add("/example/path");
        $df->save();
        $this->assertTrue(TRUE);
    }

    /** DbFile must have a load function
     *
     */
    public function testLoad() {
        $df = new DbFile(true);
        $df->load();
        $this->assertTrue(TRUE);
    }

    /** Loading should add to internal storage
     *
     */
    public function testLoadCount() {
        $df = new DbFile(true);
        $df->load();
        $this->assertEquals( $df->size(), 1);
    }

    /** Creating the DfFile instance should auto-load content
     *
     */
    public function testAutoLoad() {
        $df = new DbFile(true);
        $this->assertEquals( $df->size(), 1);
    }


    public function testIsIndexed() {
        $df = new DbFile(true);
        $this->assertEquals( $df->isIndexed("/newpath/"), false);
        $this->assertEquals( $df->isIndexed("/example/path"), true);
    }

    public function testGetGaleryPath() {
        $df = new DbFile(true);
        $this->assertEquals( $df->getGalleryPath("1c8086d5"), "/example/path");
        // Then, an inexisting one
        $this->assertEquals( $df->getGalleryPath("1c128086d5"), null);
    }    

    /** Test the prices handling (the third column */
    public function testPrices() {
        $path = "/example/path";
        $df = new DbFile(true);
        $format = 'f';
        $price = 23.02;
        $comment = 'c';
        $prices = '["' . $format . '", ' . $price . ', "' . $comment . '"]';
        $hash = $df->getPathHashsum($path);
        $df->setPrices($hash, $prices);
        $df->save();

        $df2 = new DbFile(true);
        $prices2 = $df2->getPrices($path);
        $this->assertEquals( $prices, $prices2);
    }

    /** At second read, sometimes we have more quotes around prices array
      *
      * Even it's a very similar testPrices test, it's not the same.
      *
      */
    public function testPrices_secondRead() {

        $path = "/example/path";
        $df = new DbFile(true);
        $prices = $df->getPrices($path);
        $hash = $df->getPathHashsum($path);
        $df->setPrices($hash, $prices);
        $df->save();

        $df2 = new DbFile(true);
        $prices2 = $df2->getPrices($path);
        $this->assertEquals( $prices, $prices2);
        
    }

    /** The prices string shouldn't bu an empty string */
    public function testPrices_PricesStringNotEmpty() {

        $path = "/example/path";
        $df = new DbFile(true);
        $prices = $df->getPrices($path);
        $this->assertNotEmpty( $prices );
    }

    /** The prices string shouldn't be an empty string */
    public function testPrices_PricesStringQuotes() {

        $path = "/example/path";
        $df = new DbFile(true);
        $prices = $df->getPrices($path);
        //  sould be 4 : two double quote for each string
        $this->assertEquals( substr_count($prices, '"'), 4 );
    }
    
        /** The JSON decoded array from DbFile shouldn't be null */
    public function testPrices_ArrayNotNull() {

        $path = "/example/path";
        $df = new DbFile(true);
        $prices = $df->getPrices($path);
        $pricesArray = json_decode($prices);
        $this->assertNotNull( $pricesArray );
    }

    /** Try to modify a price and add it again */
    public function testPrices_ModifyPrice() {
        $path = "/example/path";
        $df = new DbFile(true);
        $prices = $df->getPrices($path);
        $pricesArray = json_decode($prices);
        array_push($pricesArray, "[prix, 17");
        
        $hash = $df->getPathHashsum($path);
        $df->setPrices($hash, $prices);
        $df->save();

        $df2 = new DbFile(true);
        $prices2 = $df2->getPrices($path);
        $this->assertEquals( $prices, $prices2);
    }
}

?>
