<?php

require_once(dirname(__FILE__).'/../ImageHandlerBase.php');

use PHPUnit\Framework\TestCase;

/** We have to override the abstract classand implement some function to
  *  be able to use and test it
  *
  */
class ImageHandlerExample extends ImageHandlerBase {
    // Abstract methods implementations
    public function generateThumbnail($img)  { }
    public function generateThumbnailWithLayer($img, $out)  { }

}

final class ImageHandlerBaseTest extends TestCase
{

    
    /**
     * Just to test the function exists and really change the time
     */
    public function testKeepTime()
    {
        $img='media/example/exif.jpg';
        $out='media/example/test-keeptime.jpg';

        touch($out);
        $this->assertNotEquals(filemtime($img), filemtime($out));
        
        $ihe = new ImageHandlerExample();
        $ihe->keepThumbDate($img, $out);

        // Should have change the date/time of second file
        $this->assertEquals(filemtime($img), filemtime($out));
        
    }
}
?>
