<?php
require_once(dirname(__FILE__).'/../api/GetHelper.php');

use PHPUnit\Framework\TestCase;

/** A test class used with ApiResponse */
final class GetHelperTest extends TestCase
{
    public function testApiResponse() {
        $res = new ApiResponse; 
        $this->assertNotEmpty( $res );
        $this->assertEquals( $res->contentLength(), 0 );
    }

    public function testAddFile() {
        $res = new ApiResponse; 
        $res->addFile("aze");
        $this->assertEquals( $res->contentLength(), 1 );
        
    }

    /** Test with an empty directory (only an hidden file to keep
      * that directory on git)
      *
      */
    public function testGetEmptyDirectory() {
        $res = new ApiResponse;
        $pa = "src/tests/fakeEmptyGallery";
        $res->addDirectory($pa, 10);
        
        $this->assertEquals( $res->fullpath, $pa);
        $this->assertEquals( $res->contentLength(), 0 );
    }

    /** Test with a directory filled with empty files
      *
      */
    public function testGetDirectory() {
        $res = new ApiResponse;
        $pa = "src/tests/fakeGallery";
        $res->addDirectory($pa, 10);
        
        $this->assertEquals( $res->fullpath, $pa);
        $this->assertGreaterThan( 0, $res->contentLength() );
    }


    public function testGetContent() {
        $res = new ApiResponse;
        $pa = "src/tests/fakeGallery";
        $res->addDirectory($pa, 10);
        
        $this->assertEquals( $res->getContent()[0], "photo_01");
    }


    /** Test that first image is different with different offset.
      *
      */
    public function testGetDirectoryWithOffset() {
        $pa = "src/tests/fakeGallery";

        $res = new ApiResponse;
        $res->addDirectory($pa, 10, 5);

        $this->assertNotEquals($res->getContent()[0], null);
    }

    
    /** Test that first image is different with different offset.
      *
      */
    public function testGetDirectoryCompareWithOffset() {
        $pa = "src/tests/fakeGallery";

        $res1 = new ApiResponse;
        $res1->addDirectory($pa, 10);

        $res2 = new ApiResponse;
        $res2->addDirectory($pa, 10, 5);

        
        $this->assertNotEquals($res1->getContent()[0], $res2->getContent()[0]);
    }

    /** Test that first the name get with offset is correct
      *
      */
    public function testGetDirectoryWithOffsetAndName() {
        $pa = "src/tests/fakeGallery";

        $res = new ApiResponse;
        $res->addDirectory($pa, 10, 5);

        $this->assertEquals($res->getContent()[0], "photo_06");
    }

    /** Test that a out of ound offset returns a numm list
      *
      */
    public function testGetDirectoryWithOutOfBounndOffset() {
        $pa = "src/tests/fakeGallery";

        $outofbound = 150;
        
        $res = new ApiResponse;
        $res->addDirectory($pa, 10, $outofbound);

        $this->assertNotEquals($res->getContent(), null);
    }

        /** Test that a out of ound offset returns a numm list
      *
      */
    public function testApiResponseSetPaths() {
        $p1 = "path1";
        $p2 = "path2";
        $res = new ApiResponse;
        $res->setPaths($p1, $p2);
        
        $this->assertEquals($p1, $res->thumbpath);
        $this->assertEquals($p2, $res->fullpath);
    }

    /** Test with a directory filled with empty files
      *
      */
    public function testGetDirectoryContentLength() {
        $res = new ApiResponse;
        $pa = "/home/rainbru/gallery/SpeedTest/exemple/";
        $res->addDirectory($pa, 100);
        
        $this->assertEquals( $res->fullpath, $pa);
        $this->assertEquals( 100, $res->contentLength() );
    }
    
}

?>
