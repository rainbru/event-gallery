<?php
require_once(dirname(__FILE__).'/../ImageHandlerConvert.php');

use PHPUnit\Framework\TestCase;


/**
 *
 * Do not forget to call DbFile constructor with the *true* parameters
 * to use the test file.
 */

final class ImageHandlerTest extends TestCase
{

    /**
     * Just to test we haven't error instanciating ImageHandler
     */
    public function testConstructor()
    {
        $ih = new ImageHandlerConvert();
        $this->assertNotEmpty( $ih );
    }

    public function testGetThumbPath() {
        $ih = new ImageHandlerConvert();
        $img = '/home/rainbru/gallery/clermont/samedi/IMG_0001.jpg';
        $exp = '/home/rainbru/gallery_thumbs/clermont/samedi/IMG_0001.jpg';
        $this->assertEquals($ih->getThumbPath($img), $exp);
        
    }

    public function testCreateThumbDir() {
        $ih = new ImageHandlerConvert();
        $dir= '/home/rainbru/gallery_thumbs/clermont/samedi/';
        $ih->createThumbDir($dir);
        $this->assertEquals(is_dir($dir), true);
        
    }

    public function testGenerateThumbnail() {
        $ih = new ImageHandlerConvert();
        $img = '/home/rainbru/gallery/clermont/samedi/IMG_0001.jpg';
        $th = $ih->getThumbPath($img);
        //        $ih->generateThumbnail($img);
        //        $this->assertEquals(file_exists($th), true);
        $this->assertTrue(true);
    }

    /** Test that the base constructor got the RZ values from the config file
      *
      *
      *
      */
    public function testConstructorGetRzValues() {
        $ih = new ImageHandlerConvert();
        $this->assertEquals($ih->getRzQuality(), 90);
        
    }

    /** ImageHandler must have a getRzToOutputPath that takes a string
      * as argument and return a string 
      *
      */
    public function testGetRzToOutputPath() {
        $ih = new ImageHandlerConvert();
        $this->assertEquals($ih->getRzToOutputPath("/home/rainbru/rz/input/_"),
                            "/home/rainbru/rz/output/_");
    }

    public function testReadExif() {
        $photo = 'media/example/exif.jpg';
        $exif = exif_read_data($photo, 0, true);
        $dto = $exif['EXIF']['DateTimeOriginal'];
        $this->assertRegexp('/2019:05/', $dto);
    }
    
}
?>
