<?php

require_once(dirname(__FILE__).'/../Utils.php');

use PHPUnit\Framework\TestCase;

final class UtilsTest extends TestCase
{

    /**
     * The price_to_str function exists
     */
    public function testPrice_to_str()
    {
        $this->assertNotEmpty( price_to_str(10.25) );
    }

    /** 
     *
     */
    public function testPrice_to_str_Two_digits()
    {
        // Test that it makes a 4 char logn result. It should test we
        // return a two digit string
        
        $str = price_to_str(0.1);
        $this->assertEquals( strlen($str), 8 );

        // Last digit is a '0'
        //        $this->assertEquals( substr($str, -5, 1), '0' );

        // Last char is a € symbol
        //        $this->assertEquals( substr($str, -1), '€' );
        
        // Uses a comma as separator
        $this->assertEquals( substr($str, 1, 1), ',' );
    }

}
