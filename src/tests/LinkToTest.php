<?php

require_once(dirname(__FILE__).'/../LinkTo.php');

use PHPUnit\Framework\TestCase;

final class LinkToTest extends TestCase
{

    /**
     * Test thet the RZfile function takes a ImageHandler as second argument.
     */
    public function testLinkToReturnSomething()
    {
        $s = array(
            'HTTPS' => 'on',
            'HTTP_HOST' =>'test',
            'HTTP_PORT' => 80,
            'REQUEST_URI' => 'aze'
        );
        $lt = link_to('admin/index.php', $s);
        $this->assertNotEmpty( $lt );
    }

    // Tests that we keep the port thing
    public function testLinkToPort()
    {
        $s = array(
            'HTTPS' => 'on',
            'HTTP_HOST' =>'test',
            'HTTP_PORT' => 1080,
            'REQUEST_URI' => 'aze'
        );
        $lt = link_to('admin/index.php', $s);
        $this->assertNotRegExp( '/:1080/', $lt );
    }

    // Tests that we handle a non-set HTTP_PORT key
    public function testLinkToNoPort()
    {
        $s = array(
            'HTTPS' => 'on',
            'HTTP_HOST' =>'test',
            'REQUEST_URI' => 'aze'
        );
        $lt = link_to('admin/index.php', $s);
        $this->assertNotRegExp( '/:80/', $lt );
    }
    
    // Tests that we handle a non-set HTTP_PORT key
    public function testGetAbsolutePath()
    {
        $lt = get_absolute_path('admin/../index.php');
        $this->assertRegExp( '/^index.php/', $lt );

    }
}
?>
