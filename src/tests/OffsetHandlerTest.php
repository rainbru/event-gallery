<?php
require_once(dirname(__FILE__).'/../api/GetHelper.php');

use PHPUnit\Framework\TestCase;

final class OffsetHandlerTest extends TestCase
{

    public function testOffset() {
        $ofh = new OffsetHandler(0); 
        $this->assertEquals( $ofh->getOffset(), 0 );
    }
    
    public function testOffset5() {
        $ofh = new OffsetHandler(5); 
        $this->assertEquals( $ofh->getOffset(), 5 );
    }

}

?>
