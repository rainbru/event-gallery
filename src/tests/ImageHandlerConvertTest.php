<?php

require_once(dirname(__FILE__).'/../ImageHandlerConvert.php');

use PHPUnit\Framework\TestCase;

final class ImageHandlerConvertTest extends TestCase
{

    /**
     * Check that creating a thumbnail also copy EXIT to mtime
     */
    public function testEXIFvsMtime()
    {
        $img='media/example/exif.jpg';
        $out='media/example/test-output.jpg';
        
        $ih = new ImageHandlerConvert();
        $ih->generateThumbnailWithLayer($img, $out);
        
        $idt=date("Y:m:d H:i:s", filemtime($out));
        $this->assertEquals( $ih->getDto($img), $idt );
    }

}
?>
