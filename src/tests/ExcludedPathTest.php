<?php
require_once(dirname(__FILE__).'/../ExcludedPath.php');

use PHPUnit\Framework\TestCase;

final class ExcludedPathTest extends TestCase
{

    /**
     * Just to test the number of excluded paths
     */
    public function testSize()
    {
        $ep = new ExcludedPath();
        $this->assertEquals( $ep->size(), 2);
    }


    public function testExcludedTrue()
    {
        $ep = new ExcludedPath();
        $this->assertEquals( $ep->isExcluded("aze"), true);
    }

    public function testExcludedFalse()
    {
        $ep = new ExcludedPath();
        $this->assertEquals( $ep->isExcluded("aze123"), false);
    }

}
