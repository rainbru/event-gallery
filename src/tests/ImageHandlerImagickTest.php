<?php

require_once(dirname(__FILE__).'/../ImageHandlerImagick.php');

use PHPUnit\Framework\TestCase;

final class ImageHandlerImagickTest extends TestCase
{
    /** A example image known to contain valid EXIF data */
    protected $img='media/example/exif.jpg';


    public function testGetDto()
    {
        $ih = new ImageHandlerImagick();
        $dto = $ih->getDto($this->img);
        $this->assertEquals( $dto, '2019:05:21 18:03:57');
    }
    
    /**
     * Check that creating a thumbnail also copy EXIF to mtime
     */
    public function testEXIFvsMtime()
    {
        $out='media/example/test-output.jpg';
        
        $ih = new ImageHandlerImagick();
        $ih->generateThumbnailWithLayer($this->img, $out);
        
        $idt=date("Y:m:d H:i:s", filemtime($out));
        $this->assertEquals( $ih->getDto($this->img), $idt );
    }
}
?>
