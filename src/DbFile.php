<?php

/** Basically a CSV file, used to store CRC hashsum to directory pairs.
 *
 */
class DbFile { 

    /** Full path to the DB file 
     *
     */
    private $path;


    /** The algorythm used to compute hashsum
     *
     */
    private $hash_algo;

    /** 
     * The internal storage is a key:value array (for the path)
     */
    private $storage = array();
    
    /** 
     * The internal prices storage is a key:value, key is the path
     */
    private $prices = array();
    
    /** Constructor
     *
     * \param $test An argument used for unit tests only.
     *
     */
    function __construct($test = false) {
        $this->path      = parse_ini_file ('eg.ini')['dbfile_path'];
        $this->hash_algo = parse_ini_file ('eg.ini')['hash_algo'];

        if ($test) {
            $this->path = "/builds/solebull/event-gallery/gallery.db-test";
            if (!file_exists($this->path)) {
                // It shouldn't exist on CI : create it
                $my_file = fopen($this->path, "w");
                fwrite($my_file, "/example/path,1c8086d5,\n");
                fclose($my_file);
            }
        }

        $this->load();
    }

    /** A simple path getter 
     *
     */
    function getPath() {
        return $this->path;
    }

    /** Return the Hashsum for a complete path
     *
     * \param $path The path
     *
     */
    function getPathHashSum($path) {
        // The chance of collision is somewhere around 0.0016% with CRC32
        return hash($this->hash_algo, $path, false);
    }

    /** 
     * Returns the number of paths stored in the internal storage
     *
     */
    function size() {
        return count($this->storage);
    }

    /** 
     * Add the given path and its hashsum to the internal storage
     * 
     * \param $path The path to be added.
     *
     */
    function add($path) {
        $this->storage[$path] = $this->getPathHashSum($path);
    }


    /** 
     * Save the internal storage to file
     *
     * \return true if file saved, false if we failed to open DfFile
     *
     */
    function save() {
        $fp = fopen($this->path, 'w');
        if ($fp===false) {
            echo("WARNING: error writing DbFile '$this->path'\n");
            return false;
        }
            
        foreach ($this->storage as $key => $value) {
            $prices = $this->prices[$key];
            if (fputcsv($fp, array($key, $value, $prices))===false) {
                echo ("Failed to write dbfile field\n");
            }
        }

        fclose($fp);
        return true;
    }

    /** Loads the database file
      *
      */
    function load() {
        if (($handle = fopen($this->path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $path = $data[0];
                $this->storage[$path] = $data[1];
                if (count($data) > 2) {
                    $this->prices[$path] = $data[2];
                } else {
                    //  echo ("WARNING: Can't read price for path '$path'.\n");
                    $this->prices[$path] = "";
                }
            }
        }
        fclose($handle);
    }

    /** Is the given path already indexed
     *
     * \param $path The path to be tested.
     *
     */
    function isIndexed($path) {
        return array_key_exists($path, $this->storage);
    }

    /** Get a gallery path from its hashsum
     *
     * \param $hashsum A path's hashsum.
     *
     */
    function getGalleryPath($hashsum) {
        return array_search($hashsum, $this->storage);
    }

    /** Set or modify the prices string for the given hashsum 
     *
     *
     */
    function setPrices($hashsum, $pricesString) {
        //        echo "Adding prices '$hashsum' -> '$pricesString'\n";
        $path = $this->getGalleryPath($hashsum);
        $this->prices[$path] = $pricesString;
    }

    /** Returns a non decoded version of the price array */
    function getPrices($galleryPath) {
        if (array_key_exists($galleryPath, $this->prices)) {
            // print("<pre>".print_r($this->prices,true)."</pre>");
            $arraystring=$this->prices[$galleryPath];
            //        $prices = json_decode($arraystring);
            return $arraystring;
        } else {
            echo ("WARNING: Can't get price for path '$galleryPath'.\n");
        }
    }
}

?>
