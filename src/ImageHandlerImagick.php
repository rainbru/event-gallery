<?php

require_once('ImageHandlerBase.php');

/** The Imagick class-based thumbnail generation class
 *
 */
class ImageHandlerImagick extends ImageHandlerBase {

    /** Generate the thumbnail using Imagick class
     *
     * \param $img the full path to the to-be-resized image.
     *
     */
    public function generateThumbnail($img)
    {
        if (is_file($img)) {
            $output = $this->getThumbPath($img);
            if (strcmp($input,$output) == 0) {
                $msg="generateThumbnail() is about to override original image.";
                throw new Exception($msg);
                return;
            }

            $imagick = new Imagick(realpath($img));
            $imagick->setImageFormat('jpeg');
            $imagick->setImageCompression(Imagick::COMPRESSION_JPEG);
            $imagick->setImageCompressionQuality($this->thumbquality);
            $imagick->setbackgroundcolor('rgb(64, 64, 64)');
            $imagick->thumbnailImage($this->thumbwidth, $this->thumbheight,
                                     true, true);
            $imagick->writeImage($output);
            parent::keepThumbDate($img, $output);
        }
        else {
            throw new Exception("{$img} is not a valid image.");
        }
    }

    public function generateThumbnailWithLayer($img, $out)
    {
        $layer = new Imagick($this->RZ_layer);

        $imagick = new Imagick($img);
        $imagick->setImageFormat('jpeg');
        $imagick->setImageCompression(Imagick::COMPRESSION_JPEG);
        $imagick->setImageCompressionQuality($this->thumbquality);
        $imagick->resizeImage($this->RZ_width, $this->RZ_height,
                              imagick::FILTER_CUBIC, 0.5, true);
        $imagick->compositeImage($layer, Imagick::COMPOSITE_DEFAULT, 0, 0);
        $imagick->writeImage($out);
        $imagick->destroy();
        parent::changeThumbDate($img, $out);

    }

}
