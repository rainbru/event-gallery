# event-gallery

## Installation 

### Debian

To install on debian with apache :

	sudo ln -s $PWD/src/ /var/www/event-gallery

Then, point your favorite browser to https://127.0.0.1/event-gallery/

### Arch

1.Dependencies installation :

        sudo pacman -S apache php-apache php-fpm

2.If you want to modify system-wide index file :

        sudo emacs /srv/http/index.html

3.Then, edit the */etc/hosts* file to add this line:

        127.0.0.1  eventg.localhost

4.Enable *virtual hosts* and *php* apache features. Open 
*/etc/httpd/conf/httpd.conf* file and uncomment or add this statements :

        Include conf/extra/httpd-vhosts.conf
        Include conf/extra/php_module.conf
        ...
        LoadModule php_module modules/libphp.so
        AddHandler php-script .php

5.Open The vhost configuration file `/etc/httpd/conf/extra/httpd-vhosts.conf`,
        and add this content. Be sure to change both directory path if needed :

        <VirtualHost *:80>
                ServerAdmin webmaster@domainname1.dom
                DocumentRoot "/var/www/eventg/src"
                ServerName eventg.locahost
                ServerAlias eventg.locahost
                ErrorLog "/var/log/httpd/eventg.dom-error_log"
                CustomLog "/var/log/httpd/eventg.dom-access_log" common

            <Directory "/var/www/eventg/src">
                        Require all granted
                        DirectoryIndex index.php
                </Directory>
        </VirtualHost>

Now, you can start (or restart apache) : `sudo systemctl start httpd.service`.


## QNAP installation


After calling the `./build.sh` script, you should have two 
*event-gallery-{version}* files, with the .qpkg and .zip extension.

### ImagaMagick installation

For imagemagick installation on QNAP : 
	https://forum.qnap.com/viewtopic.php?p=175896#p175896
	
	ipkg update .... to update the list of packages
	ipkg install imagemagick ... to install package

### QPKG installation

The .qpkg file installation is buggy but usefull to add an application
shortcut.

### ZIP installation

1. On the NAS, open **FileStation 5**;
2. Go to QNAP > DataVol1 > Web;
3. Manually upload event-gallery-{version}.zip file;
4. right click on the uploaded file and chose *Extract > Extract here*.

## Site configuration

Modify the `/etc/hosts` and add this file :

	127.0.0.1	event-gallery.localhost.org


Add to `/etc/apache2/sites-available/event-gallery.conf` file :

	<VirtualHost *:80>
		ServerName templequite-hp.localhost.org
		DocumentRoot /var/www/event-gallery
		AddType application/x-httpd-php .php
    <Directory /var/www/event-gallery>
		Order allow,deny
			allow from all
	AllowOverride All
    </Directory>

	</VirtualHost>
	
then :

	sudo a2ensite event-gallery
	systemctl reload apache2

## Configuration file

*src* directory contains a **eg.ini.example** you must first rename to
*eg.ini* then modify to be able to use *event-gallery*.

### `thumb_method`

This configuration key is used to select the method used to generate
screenshots. You can choose betwen :

	imagick:     To use the PHP image magick class.
	convert:     To use convert system command.
	

### `pl_*`

Partial is/was an experimental feature, tried to lower loading times. It's
buggy so it can be deactivated. See ROADMAP file for more debugging and to know
remaining work.

If disabled, the API is deactivated and will return a 403 error status.

## Modify admin password

	cd src/admin/
	htpasswd -c .htpasswd <login_name>

et répeter deux fois le mot de passe.

Vous devez aussi modifier l'emplacement de l'*AuthUserFile* dans le fichier
`htaccess`.

## Publicly accessible images

The images and thumbs shouls ba accessible as :
	
	src/public/
	src/public/thumbs
	

## Templates

We're using our own template engine.

It understand `{{}}` substitutions and full PHP code :

	<?php echo $DIR; ?>

By convention, but it's not mandatory, all substitued variable are uppercase.

# QDK icons
	
Icons are located in `qpkg/icon`

From the [official documentation](http://download.qnap.com/dev/QDK_2.0.pdf):

	Location of directory with icons for the packaged software. Default 
	location is a directory named icons in $QDK_ROOT_DIR. The value must 
	be a full path or a path relative to $QDK_ROOT_DIR.
	The icons shall be named ${QPKG_NAME}.gif, ${QPKG_NAME}_80.gif, and 
	${QPKG_NAME}_gray.gif,
	
	* ${QPKG_NAME}.gif is the image displayed in the web interface when the 
	QPKG is enabled. It should be a GIF image of 64x64 pixels.
	* ${QPKG_NAME}_gray.gif is the image displayed in the web interface when 
	the QPKG is disabled. It should be a GIF image of 64x64 pixels. It is 
	usually a greyscale version of the ${QPKG_NAME}.gif image, but that is 
	not a requirement.
	* ${QPKG_NAME}_80.gif is the image displayed in the pop-up dialog (with 
	information about the QPKG and the buttons to enable, disable, and 
	remove). It should be a GIF image of 80x80 pixels.
	If no icons are included then the QPKG is given default icons at 
	installation

# Running unit tets locally

We've some important unit tests here. To run them, call `make check`.

To run them locally (there're configured to run at gitlab), you must
create the same directory :

	su
	mkdir -p /builds/solebull/event-gallery/
	chmod 777 /builds/solebull/event-gallery/

# QDK installation

On debian, you can use dpkg package from 

	https://github.com/qnap-dev/qdk2/releases

After installation, you have to manually move or link app to :

	ln -s /share/XXX_DATA/.qpkg/QDK/MyQPKG/shared/web /share/Web/event-gallery

# Limitation

* The hashsum is generated using the directory name as entropy : please prefer
  long, descriptive names;
* Directory names can't containe *"* characters;

# Known issues

## Bad request issue

If your browser is showing you a *Bad Request* error when in a sub-gallery page,
please ensure you're in the `https://` version of the website.

## Trailing ? error

The gallery viewer may end with a *Not found* issue even if a `&c=`'style
parameter is present in the URL if the final URL contains two parameters 
blocks :

	/event-gallery/?id=30789a99/?
	
for example.

A known fix is to add a final **&** at the end of the URL before using an 
URL shortening service.

	/event-gallery/?id=30789a99&

# API

The api has only ont endpoint :

## photo/get

Get a list of photo from a given gallery using the given parameters :

### Parameters

| **Parameter** | **Type** | **Descrption**                 |
|---------------|----------|--------------------------------|
| crc           | string   | The gallery CRC hashsum.       |
| c             | string   | The gallery subdirectory name. |
| pid           | integer  | The last loaded image offset.  |
| nb            | integer  | The number og image to load.   |

### Example output

Here is a sample output :

    {
      "status": {
        "status_code":200,
        "status_text":"OK",
        "status_comment":""
      },"answer":{
        "path":"http:\/\/<example-url   ",
        "content":[
           "image001.jpg",
           "image002.jpg",
           "image003.jpg",
           "image004.jpg",
           "image005.jpg",
           "image006.jpg",
           "image007.jpg",
           "image008.jpg"
         ]
       }
     }
