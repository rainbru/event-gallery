#!/bin/sh

suff="-beta" 


# According to the actual git branch we're creating the beta zip or
# the final app one.
branch=`git branch | grep \* | cut -d ' ' -f2`
echo "We're on '$branch' branch"
if [ "$branch" = "master" ]; then
   suff=""
   branch="On master branch. Creating non-beta archive"
fi

dirname="event-gallery$suff"

# Early exit if $dirname is empty (could break sources)
if [ -z "$dirname" ]; then
    echo "ERROR: Check the script, $dirname is empty."
    exit 0
fi
      
rm -fr $dirname/
rm -fr qpkg/shared/Web/$dirname
cp -R --no-dereference src/ $dirname/
rm -fr $dirname/src/public
rm -fr $dirname/src/
rm -fr $dirname/public
rm -fr $dirname/tests
rm -fr $dirname/example
rm -fr $dirname/*~
rm -fr $dirname/*/*~
rm -fr $dirname/*/*/*~
rm -fr $dirname/media/img-src/
rm -fr $dirname/img-src/

rm $dirname/eg.ini
mv $dirname/greg-eg.ini $dirname/eg.ini
rm $dirname/eg.ini.example

cp -R $dirname/ qpkg/shared/Web/

# Handle greg's config file
cd qpkg/
/usr/share/qdk2/QDK/bin/qbuild
/usr/share/qdk2/QDK/bin/qbuild --sign
mv build/event-gallery_0.1.0.qpkg ..


# Zip version
cd ..
# Simply move the directopry created for QPKG building
zip -r $dirname-0.2.0.zip $dirname
rm -fr $dirname/
