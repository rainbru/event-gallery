# Branches

Event-gallery development use `git` branch to work on multiple application
states.

* `event-gallery` : the final app. is always on *master* git branch;
* `event-gallery-beta`: a beta version with non mature or work in progress 
  features.
  
The `build.sh` script knows how to generate the working version.
