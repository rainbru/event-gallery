# CreateLayerDir : manuel

*CreateLayerDir* est outils pour copier une arborescence entière, les images
étant redimensionnées ern y ajoutant un *layer*.

## Installation

- Vous devez éxecuter *Windows Power Shell ISE* et y activer l'execution
de script 

		Set-ExecutionPolicy -scope CurrentUser RemoteSigned

- Si le code sur la partie gauche est illisible, passer windows en contraste 
  elevé.


# Utilisation

*CreateLayerDir* est un outils en ligne de commande. Pour le lancer :

1. Executer *Windows Power Shell ISE*
2. Si vous utiliser le dossier *Documents*, son contenu peut être trouvé dans
   *C:\Users\<votre_nom>\Documents*

exemple : 
