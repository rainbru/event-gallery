#+OPTIONS: toc:nil

* Event-gallery : manuel

Utilisation complète et mise en ligne d'une galerie. Elle se passe en deux 
étapes :
- RZ + layout : pour redimensionner et ajouter un filigrane aux photos;
- L'indexation depuis l'interface d'administration.

Cette organisation remplit deux objectifs :
- Le client n'a jamais acces aux photos originales (tailles completes sans 
  filigrane);
- Les photos originales ne sont jamais modifiées, le travail s'effectuant
  toujours sur des copies.

** 1. RZ + layout

Cette étape a pour but de créer des photos de grande taille plus petites que les
originales et d'ajouter un filigrane (/layer/).

- S'assurer que le repertoire de sortie de RZ 
  (=/share/CACHEDEV1_DATA/Public/RZ/RZ_output=) n'existe pas
- Deplacer les galeries comme des sous-dossiers dans 
  =/share/CACHEDEV1_DATA/Public/RZ/RZ_input=.
- Lancer le process en appuyant sur le bouton **Redimensionner**.

- **Attendre!**

Si le bouton est grisé, vérifier que tous les commentaires suivant les valeurs
sont bien verts.

** 2. Indexation

- Déplacer la galerie redimensionnée depuis 
  =/share/CACHEDEV1_DATA/Public/RZ/RZ_output= vers le repertoire de 
  competitions brutes (=/share/CACHEDEV1_DATA/Web/competitions_brutes/=);
- Recharger la page d'administration d'*event-gallery*;
- Appuyer sur le bouton **Update galleries**.

- **Attendre!**

Note : il est très important de déplacer (*mv*) et non copier. Sous linux, 
déplacer ne bouge que l'*inode* parent et évitera de copier les 2000 photos
contenues dans le dossier;

Note 2 : au moment de copier l'URL vers un service de masquage de lien, si
le dit service a tendance a ajouter un '?' supplémentatire dans l'URL,
coller l'URL avec un '&' final :

=/event-gallery/?id=30789a99&=
